
duels = {
    edit = {},
    gridlist = {},
    label = {},
    button = {},
    window = {},
    scrollbar = {},
    combobox = {}
}
duels.window[1] = guiCreateWindow(0.72, 0.29, 0.27, 0.55, "Выбор оппонента", true)
guiWindowSetSizable(duels.window[1], false)

duels.gridlist[1] = guiCreateGridList(0.03, 0.04, 0.90, 0.57, true, duels.window[1])
guiGridListAddColumn(duels.gridlist[1], "Игроки", 0.3)
guiGridListAddColumn(duels.gridlist[1], "Победы", 0.3)
guiGridListAddColumn(duels.gridlist[1], "Поражения", 0.3)
for i = 1, 2 do
    guiGridListAddRow(duels.gridlist[1])
end
guiGridListSetItemText(duels.gridlist[1], 0, 1, "-", false, false)
guiGridListSetItemText(duels.gridlist[1], 0, 2, "-", false, false)
guiGridListSetItemText(duels.gridlist[1], 0, 3, "-", false, false)
guiGridListSetItemText(duels.gridlist[1], 1, 1, "-", false, false)
guiGridListSetItemText(duels.gridlist[1], 1, 2, "-", false, false)
guiGridListSetItemText(duels.gridlist[1], 1, 3, "-", false, false)

duels.scrollbar[1] = guiCreateScrollBar(0.94, 0.05, 0.05, 0.92, false, true, duels.gridlist[1])

duels.combobox[1] = guiCreateComboBox(0.03, 0.66, 0.58, 0.32, "", true, duels.window[1])
guiComboBoxAddItem(duels.combobox[1], "test")
duels.button[1] = guiCreateButton(0.66, 0.85, 0.29, 0.13, "Отправить запрос", true, duels.window[1])
guiSetFont(duels.button[1], "default-bold-small")
duels.edit[1] = guiCreateEdit(0.66, 0.69, 0.26, 0.04, "", true, duels.window[1])
duels.label[1] = guiCreateLabel(0.67, 0.64, 0.26, 0.04, "Кол-во HP:", true, duels.window[1])
guiSetFont(duels.label[1], "default-bold-small")
duels.label[2] = guiCreateLabel(0.14, 0.62, 0.31, 0.04, "Выберите оружие:", true, duels.window[1])
guiSetFont(duels.label[2], "default-bold-small")
