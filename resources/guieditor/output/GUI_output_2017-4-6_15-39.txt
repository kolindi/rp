
authGUI = {
    tab = {},
    tabpanel = {},
    radiobutton = {},
    button = {},
    window = {},
    label = {}
}
authGUI.window[2] = guiCreateWindow(0.75, 0.31, 0.24, 0.54, "Кастомизация", true)
guiWindowSetMovable(authGUI.window[2], false)
guiWindowSetSizable(authGUI.window[2], false)

authGUI.tabpanel[1] = guiCreateTabPanel(0.03, 0.05, 0.93, 0.74, true, authGUI.window[2])

authGUI.tab[1] = guiCreateTab("Внешность", authGUI.tabpanel[1])

authGUI.button[4] = guiCreateButton(0.25, 0.13, 0.54, 0.18, "Следующий", true, authGUI.tab[1])
guiSetFont(authGUI.button[4], "default-bold-small")
authGUI.button[5] = guiCreateButton(0.24, 0.45, 0.55, 0.18, "Предыдущий", true, authGUI.tab[1])
guiSetFont(authGUI.button[5], "default-bold-small")

authGUI.tab[2] = guiCreateTab("Умение", authGUI.tabpanel[1])

authGUI.label[1] = guiCreateLabel(0.13, 0.03, 0.71, 0.05, "Вы можете выбрать одно умение", true, authGUI.tab[2])
guiSetFont(authGUI.label[1], "default-bold-small")
authGUI.radiobutton[3] = guiCreateRadioButton(0.61, 0.15, 0.06, 0.04, "", true, authGUI.tab[2])
authGUI.label[2] = guiCreateLabel(0.02, 0.15, 0.50, 0.04, "Стрельба из пистолетов", true, authGUI.tab[2])
authGUI.label[3] = guiCreateLabel(0.03, 0.70, 0.49, 0.09, "50000Kol\n(Колиндов - валюта)", true, authGUI.tab[2])
authGUI.radiobutton[4] = guiCreateRadioButton(0.62, 0.72, 0.05, 0.04, "", true, authGUI.tab[2])
guiRadioButtonSetSelected(authGUI.radiobutton[4], true)


authGUI.button[6] = guiCreateButton(0.19, 0.83, 0.59, 0.12, "Готово!", true, authGUI.window[2])
guiSetFont(authGUI.button[6], "default-bold-small")
