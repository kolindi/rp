function engineState(playerSource)
	local car = getPedOccupiedVehicle(playerSource)
	local curent = getVehicleEngineState(car)
	if curent == true then
		setVehicleEngineState(car, false)
		setElementData(car, "engineState", 0)
	else
		setVehicleEngineState(car, true)
		setElementData(car, "engineState", 1)
	end
end

function engineOffOnDamage(loss)
	if loss > 99 then
		setVehicleEngineState(source, false)
	end
end
addEventHandler("onVehicleDamage", getRootElement(), engineOffOnDamage)

function engineAutoStartOff(playerSource, seat)
	if getElementData(source, "engineState") == 0 then
		setVehicleEngineState(source, false)
		--setElementData(source, "firstSear", 0)
	end
end
addEventHandler("onVehicleEnter", getRootElement(), engineAutoStartOff)