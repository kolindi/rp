function carHackTrigger(car)
	local stat_hack = getElementData(source, "stat_hack")
	if stat_hack == 10 then
		error_chance = 70
		hack_time = 60
	elseif stat_hack == 20 then
		error_chance = 60
		hack_time = 55
	elseif stat_hack == 30 then
		error_chance = 50
		hack_time = 40
	elseif stat_hack == 50 then
		error_chance = 30
		hack_time = 30
	elseif stat_hack == 80 then
		error_chance = 10
		hack_time = 20
	elseif stat_hack == 100 then
		error_chance = 5
		hack_time = 15
	end
	
end
addEventHandler("onClientHackCarClick", getRootElement(), carHackTrigger)