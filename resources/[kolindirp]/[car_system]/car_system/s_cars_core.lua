function engineOn(playerSource)
	if getElementData(source, "org") == false then
		setVehicleEngineState(source, true)
		setElementData(source, "engineState", 1)
	elseif getElementData(source, "org") == getElementData(playerSource, "org") then
		setVehicleEngineState(source, true)
		setElementData(source, "engineState", 1)
	else
		exports.ns:sendNotification(playerSource, "abort", "Система отказала вам в доступе!")
	end
end
addEventHandler("onClientEngineOnClick", getRootElement(), engineOn)

function engineOff(playerSource)
	setVehicleEngineState(source, false)
	setElementData(source, "engineState", 0)
end
addEventHandler("onClientEngineOffClick", getRootElement(), engineOff)

function lightOn(playerSource)
	setVehicleOverrideLights(source, 2)
	setElementData(source, "lightState", 1)
end
addEventHandler("onClientLightOnClick", getRootElement(), lightOn)

function lightOff(playerSource)
	setVehicleOverrideLights(source, 1)
	setElementData(source, "lightState", 0)
end
addEventHandler("onClientLightOffClick", getRootElement(), lightOff)

function carLockStateChange(carId)
	local car = exports.id_system:getVehicleByID(carId)
	--outputDebugString(getElementData(car, "id"))
	local x, y, z = getElementPosition(source)
	local cx, cy, cz = getElementPosition(car)
	if getDistanceBetweenPoints3D(x, y, z, cx, cy, cz) < 15 then
		if isVehicleLocked(car) == true then
			setVehicleLocked(car, false)
			exports.ns:sendNotification(source, "success", "Вы открыли машину")
		elseif isVehicleLocked(car) == false then
			setVehicleLocked(car, true)
			exports.ns:sendNotification(source, "success", "Вы закрыли машину")
		end
	else
		exports.ns:sendNotification(source, "error", "Машина слишком далеко!")
	end
end
addEventHandler("onClientCarOpenClick", getRootElement(), carLockStateChange)