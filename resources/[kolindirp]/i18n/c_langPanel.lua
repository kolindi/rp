DGS = exports.dgs
langGUI = {}
function createLangPanel(isReg)
	showCursor(true)
	langGUI.window = DGS:dgsDxCreateWindow(0.3, 0.4, 0.5, 0.3, "Select language/Выберите язык", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
	langGUI.langList = DGS:dgsDxCreateGridList(0, 0, 1, 0.8, true, langGUI.window)
	langGUI.column = DGS:dgsDxGridListAddColumn(langGUI.langList, "", 0.85)
	langGUI.rowEN = DGS:dgsDxGridListAddRow(langGUI.langList)
	langGUI.rowRU = DGS:dgsDxGridListAddRow(langGUI.langList)
	DGS:dgsDxGridListSetItemText(langGUI.langList, langGUI.rowEN, langGUI.column, "English", false, false)
	DGS:dgsDxGridListSetItemText(langGUI.langList, langGUI.rowRU, langGUI.column, "Русский", false, false)
	langGUI.button = DGS:dgsDxCreateButton(0, 0.8, 1, 0.2, "Accept/Подтвердить", true, langGUI.window)
end
addEventHandler("createLangPanel", getRootElement(), createLangPanel)
--addEventHandler("onClientResourceStart", getResourceRootElement(getThisResource()), createLangPanel)

function buttonTrigger(button, state)
	if button == "left" and state == "up" and source == langGUI.langList then
		local selected = DGS:dgsDxGridListGetSelectedItem(langGUI.langList)
		if selected ~= -1 then
			local langSelect = DGS:dgsDxGridListGetItemText(langGUI.langList, selected, langGUI.column)
			if langSelected == "English" then
				local lang = "en"
			elseif langSelected == "Русский" then
				local lang = "ru"
			end
		end
	elseif button == "left" and state == "up" and source == langGUI.button then
		if lang == "en" or lang == "ru" then
			if isReg == true then
				triggerEvent("createLoginPanel", localPlayer, lang)
			else
				triggerServerEvent("changeLang", localPlayer, lang)
			end
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), buttonTrigger)