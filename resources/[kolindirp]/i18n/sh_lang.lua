function langTest(playerSource)
	getText(getPlayerFromName("Avram"), "LSPD")
end
addCommandHandler("testLang", langTest)

function getText(playerSource, text)
	lang = getElementData(playerSource, "lang")
	if lang == "en" then
		return en[text]
	elseif lang == "ru" then
		return ru[text]
	end
end