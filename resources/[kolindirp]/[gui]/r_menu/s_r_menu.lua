--[[
function engineOn()
	setVehicleEngineState(source, true)
	setElementData(source, "engineState", 1)
end
addEventHandler("onClientEngineOnClick", getRootElement(), engineOn)

function engineOff()
	setVehicleEngineState(source, false)
	setElementData(source, "engineState", 0)
end
addEventHandler("onClientEngineOffClick", getRootElement(), engineOff)

function lightOn()
	setVehicleOverrideLights(source, 2)
	setElementData(source, "lightState", 1)
end
addEventHandler("onClientLightOnClick", getRootElement(), lightOn)

function lightOff()
	setVehicleOverrideLights(source, 1)
	setElementData(source, "lightState", 0)
end
addEventHandler("onClientLightOffClick", getRootElement(), lightOff)

function carLockStateChange(carId)
	local car = exports.id_system:getVehicleByID(carId)
	--outputDebugString(getElementData(car, "id"))
	local x, y, z = getElementPosition(source)
	local cx, cy, cz = getElementPosition(car)
	if getDistanceBetweenPoints3D(x, y, z, cx, cy, cz) < 15 then
		if isVehicleLocked(car) == true then
			setVehicleLocked(car, false)
			exports.ns:sendNotification(source, "success", "Вы открыли машину")
		elseif isVehicleLocked(car) == false then
			setVehicleLocked(car, true)
			exports.ns:sendNotification(source, "success", "Вы закрыли машину")
		end
	else
		exports.ns:sendNotification(source, "error", "Машина слишком далеко!")
	end
end
addEventHandler("onClientCarOpenClick", getRootElement(), carLockStateChange)

function carLock(carId)
	local car = exports.id_system:getVehicleByID(carId)
	--outputDebugString(getElementData(car, "id"))
	local x, y, z = getElementPosition(source)
	local cx, cy, cz = getElementPosition(car)
	if getDistanceBetweenPoints3D(x, y, z, cx, cy, cz) < 10 then
		setVehicleLocked(car, true)
		exports.ns:sendNotification(source, "success", "Вы закрыли машину")
	else
		exports.ns:sendNotification(source, "error", "Машина слишком далеко!")
	end
end
addEventHandler("onClientCarLockClick", getRootElement(), carLock)
]]