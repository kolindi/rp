DGS = exports.dgs
--local screenWidth, screenHeight = guiGetScreenSize()
RWindow = {}
function rMenuOpen()
	showCursor(true)
	--[[
	RWindow[1] = DGS:dgsDxCreateWindow(0.45, 0.10, 0.10, 0.10, "", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		DGS:dgsDxWindowSetSizable(RWindow[1], false)
		DGS:dgsDxWindowSetMovable(RWindow[1], false)
	RWindow[2] = DGS:dgsDxCreateWindow(0.35, 0.20, 0.10, 0.10, "", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		DGS:dgsDxWindowSetSizable(RWindow[2], false)
		DGS:dgsDxWindowSetMovable(RWindow[2], false)
	RWindow[3] = DGS:dgsDxCreateWindow(0.55, 0.20, 0.10, 0.10, "", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		DGS:dgsDxWindowSetSizable(RWindow[3], false)
		DGS:dgsDxWindowSetMovable(RWindow[3], false)
	RWindow[4] = DGS:dgsDxCreateWindow(0.25, 0.30, 0.10, 0.10, "", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		DGS:dgsDxWindowSetSizable(RWindow[4], false)
		DGS:dgsDxWindowSetMovable(RWindow[4], false)
	RWindow[5] = DGS:dgsDxCreateWindow(0.65, 0.30, 0.10, 0.10, "", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		DGS:dgsDxWindowSetSizable(RWindow[5], false)
		DGS:dgsDxWindowSetMovable(RWindow[5], false)
	RWindow[6] = DGS:dgsDxCreateWindow(0.35, 0.40, 0.10, 0.10, "", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		DGS:dgsDxWindowSetSizable(RWindow[6], false)
		DGS:dgsDxWindowSetMovable(RWindow[6], false)
	RWindow[7] = DGS:dgsDxCreateWindow(0.55, 0.40, 0.10, 0.10, "", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		DGS:dgsDxWindowSetSizable(RWindow[7], false)
		DGS:dgsDxWindowSetMovable(RWindow[7], false)
	RWindow[8] = DGS:dgsDxCreateWindow(0.45, 0.50, 0.10, 0.10, "", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		DGS:dgsDxWindowSetSizable(RWindow[8], false)
		DGS:dgsDxWindowSetMovable(RWindow[8], false)
		]]
	RWindow[1] = DGS:dgsDxCreateImage(0.45, 0.10, 0.10, 0.10, ":r_menu/img/cir.png", true)
	RWindow[2] = DGS:dgsDxCreateImage(0.35, 0.20, 0.10, 0.10, ":r_menu/img/cir.png", true)
	RWindow[3] = DGS:dgsDxCreateImage(0.55, 0.20, 0.10, 0.10, ":r_menu/img/cir.png", true)
	RWindow[4] = DGS:dgsDxCreateImage(0.25, 0.30, 0.10, 0.10, ":r_menu/img/cir.png", true)
	RWindow[5] = DGS:dgsDxCreateImage(0.65, 0.30, 0.10, 0.10, ":r_menu/img/cir.png", true)
	RWindow[6] = DGS:dgsDxCreateImage(0.35, 0.40, 0.10, 0.10, ":r_menu/img/cir.png", true)
	RWindow[7] = DGS:dgsDxCreateImage(0.55, 0.40, 0.10, 0.10, ":r_menu/img/cir.png", true)
	RWindow[8] = DGS:dgsDxCreateImage(0.45, 0.50, 0.10, 0.10, ":r_menu/img/cir.png", true)
	if isPedInVehicle(localPlayer) then
		playerCar = getPedOccupiedVehicle(localPlayer)
		if getVehicleEngineState(playerCar) == false then
			DGS:dgsDxGUISetText(RWindow[3], "Зав.двигатель")
			engineOnImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/engine.png", true, RWindow[3])
		elseif getVehicleEngineState(playerCar) == true then
			DGS:dgsDxGUISetText(RWindow[3], "Загл.двигатель")
			engineOffImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/engineOff.png", true, RWindow[3])
		end
		if getElementData(playerCar, "lightState") == 0 then
			DGS:dgsDxGUISetText(RWindow[7], "Вкл.Фары")
			lightOnImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/light.png", true, RWindow[7])
		elseif getElementData(playerCar, "lightState") == 1 then
			DGS:dgsDxGUISetText(RWindow[7], "Откл.Фары")
			lightOffImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/lightOff.png", true, RWindow[7])
		end
		if getElementData(localPlayer, "stat_hack") > 9 then
			DGS:dgsDxGUISetText(RWindow[6], "Взлом")
			hackImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/hack.png", true, RWindow[6])
		end
	end
	if getElementData(localPlayer, "car_id") ~= false then
		carId = getElementData(localPlayer, "car_id")
		--outputChatBox(carId)
		--local car = exports.id_system:getVehicleByID(carId)
		--if isVehicleLocked(car) == false then
			DGS:dgsDxGUISetText(RWindow[5], "Ключ авто.")
			carKeyImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/carKey.png", true, RWindow[5])
		--elseif isVehicleLocked(car) == true then
			--DGS:dgsDxGUISetText(RWindow[5], "Закрыть авто.")
			--carLockImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/carKeyLock.png", true, RWindow[5])
		--end
	end
	if getElementData(localPlayer, "inHouse") == 1 then
		DGS:dgsDxGUISetText(RWindow[3], "Выйти из дома")
		exitHouseImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/houseExit.png", true, RWindow[3])
	end
	pOrg = getPlayerTeam(localPlayer)
	pOrgType = getElementData(pOrg, "type")
	zone = getElementData(localPlayer, "zone")
	zoneOwner = getElementData(zone, "owner")
	outputChatBox(getTeamName(zoneOwner))
	zoneOwnerType = getElementData(zoneOwner, "type")
	if getElementData(localPlayer, "inHouse") ~= 1 then
		if pOrgType == 3 and zoneOwnerType == 10 or pOrgType == 3 and zoneOwnerType == 2 or pOrgType == 3 and zoneOwnerType == 1 then
			attackZoneImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/attack.png", true, RWindow[3])
		elseif pOrgType == 5 and zoneOwnerType == 3 then
			attackZoneImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/attack.png", true, RWindow[3])
		elseif pOrgType == 1 and zoneOwnerType == 2 then
			attackZoneImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/attack.png", true, RWindow[3])
		elseif pOrgType == 2 and zoneOwnerType == 1 or pOrgType == 2 and zoneOwnerType == 2 or pOrgType == 2 and zoneOwnerType == 10 then
			attackZoneImg = DGS:dgsDxCreateImage(0.15, 0, 0.70, 1, ":r_menu/img/attack.png", true, RWindow[3])
		end
	end
end
bindKey("R", "down", rMenuOpen)

function triggerAction(button, state)
	if state == "up" then
		if source == engineOnImg then
			--outputChatBox("ЫФВА")
			triggerServerEvent("onClientEngineOnClick", playerCar, localPlayer)
			rMenuClose()
		elseif source == engineOffImg then
			triggerServerEvent("onClientEngineOffClick", playerCar, localPlayer)
			rMenuClose()
		elseif source == lightOnImg then
			triggerServerEvent("onClientLightOnClick", playerCar, localPlayer)
			rMenuClose()
		elseif source == lightOffImg then
			triggerServerEvent("onClientLightOffClick", playerCar, localPlayer)
			rMenuClose()
		elseif source == carKeyImg then
			triggerServerEvent("onClientCarOpenClick", localPlayer, carId)
			rMenuClose()
		elseif source == exitHouseImg then
			triggerServerEvent("onClientExitHouseClick", localPlayer)
			rMenuClose()
		elseif source == hackImg then
			triggerServerEvent("onClientHackCarClick", localPlayer, playerCar)
			rMenuClose()
		elseif source == attackZoneImg then
			triggerServerEvent("onOrgAttackZone", pOrg, localPlayer, zoneOwner, zone)
			rMenuClose()
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), triggerAction)

function rMenuClose()
		showCursor(false)
		destroyElement(RWindow[1])
		destroyElement(RWindow[2])
		destroyElement(RWindow[3])
		destroyElement(RWindow[4])
		destroyElement(RWindow[5])
		destroyElement(RWindow[6])
		destroyElement(RWindow[7])
		destroyElement(RWindow[8])
end
bindKey("R", "up", rMenuClose)