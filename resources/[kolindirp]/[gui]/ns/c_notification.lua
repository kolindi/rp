DGS = exports.dgs
ns = {}
function createNotification(img, text)
	k = #ns
	ns[k+1] = DGS:dgsDxCreateWindow(0.65, 0.90, 0.35, 0.04, "", true, 0xFFFFFFFF, 0, nil, 0x96141414, nil, 0x96141414, 5, true)
	if img == "notif" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/notification.png", true, ns[k+1])
	elseif img == "warning" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/warning.png", true, ns[k+1])
	elseif img == "info" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/info.png", true, ns[k+1])
	elseif img == "error" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/error.png", true, ns[k+1])
	elseif img == "success" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/success.png", true, ns[k+1])
	elseif img == "abort" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/abort.png", true, ns[k+1])
	elseif img == "battle" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/battle.png", true, ns[k+1])
	elseif img == "battleSuccess" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/battleSuccess.png", true, ns[k+1])
	elseif img == "battleFail" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/battleFail.png", true, ns[k+1])
	else
		outputChatBox("Ошибка создания уведомления! Обратитесь к администратору.", 255, 0, 0)
	end
	nstext = DGS:dgsDxCreateLabel(0.11, 0.2, 0.10, 1, text, true, ns[k+1])
	setTimer(destroyNotification, 10000, 1, k+1)
	for _, nss in ipairs(ns) do
		local x, y = DGS:dgsGetPosition(nss, true)
		DGS:dgsSetPosition(nss, x, y - 0.05, true)
	end
end
addEventHandler("onPlayerGotNotification", getRootElement(), createNotification)

function destroyNotification(k)
	destroyElement(ns[k])
	ns[k] = nil
end

function sendCNotification(img, text)
	k = #ns
	ns[k+1] = DGS:dgsDxCreateWindow(0.65, 0.90, 0.35, 0.04, "", true, 0xFFFFFFFF, 0, nil, 0x96141414, nil, 0x96141414, 5, true)
	if img == "notif" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/notification.png", true, ns[k+1])
	elseif img == "warning" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/warning.png", true, ns[k+1])
	elseif img == "info" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/info.png", true, ns[k+1])
	elseif img == "error" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/error.png", true, ns[k+1])
	elseif img == "success" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/success.png", true, ns[k+1])
	elseif img == "abort" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/abort.png", true, ns[k+1])
	elseif img == "battle" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/battle.png", true, ns[k+1])
	elseif img == "battleSuccess" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/battleSuccess.png", true, ns[k+1])
	elseif img == "battleFail" then
		nsimg = DGS:dgsDxCreateImage(0, 0, 0.10, 1, ":ns/img/battleFail.png", true, ns[k+1])
	else
		outputChatBox("Ошибка создания уведомления! Обратитесь к администратору.", 255, 0, 0)
	end
	nstext = DGS:dgsDxCreateLabel(0.11, 0.2, 0.10, 1, text, true, ns[k+1])
	setTimer(destroyNotification, 10000, 1, k+1)
	for _, nss in ipairs(ns) do
		local x, y = DGS:dgsGetPosition(nss, true)
		DGS:dgsSetPosition(nss, x, y - 0.05, true)
	end
end