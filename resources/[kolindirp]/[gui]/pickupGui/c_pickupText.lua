function pickupText()
	pickups = getElementsByType("pickup", getRootElement(), true)
	for _, pickup in ipairs(pickups) do
		if getElementData(pickup, "text") ~= false then
			local text = exports.i18n:getText(localPlayer, getElementData(pickup, "text"))
			if text then
				exports.functions:dxDrawTextOnElement(pickup, text, 1, 20, 255, 255, 255, 255, 2, "default-bold")
			else
				exports.functions:dxDrawTextOnElement(pickup, getElementData(pickup, "text"), 1, 20, 255, 255, 255, 255, 2, "default-bold")
			end
		end
	end
end
addEventHandler("onClientRender", getRootElement(), pickupText)