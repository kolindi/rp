addEventHandler("onClientResourceStart", root,
	function()
		setPlayerHudComponentVisible("radar", true) -- Радар
		setPlayerHudComponentVisible("clock", false) -- Время
		setPlayerHudComponentVisible("ammo", true ) -- Патроны
		setPlayerHudComponentVisible("area_name", true ) -- Районы
		setPlayerHudComponentVisible("armour", true ) -- Броня
		setPlayerHudComponentVisible("breath", true ) -- Кислород
		setPlayerHudComponentVisible("health", true ) -- Здоровье
		setPlayerHudComponentVisible("money", true ) -- Деньги
		setPlayerHudComponentVisible("vehicle_name", true ) -- Название машины
		setPlayerHudComponentVisible("weapon", true ) -- Оружие
		setPlayerHudComponentVisible("radio", true ) -- Радио
		setPlayerHudComponentVisible("wanted", true ) -- Уровень розыска
		setPlayerHudComponentVisible("crosshair", true ) -- Прицел
	end
)
