database = call(getResourceFromName("db"), "getDb")

function autosave()
	local players = getElementsByType("player")
	local playerCount = getPlayerCount()
	local savedPlayers = 0
	for key, thePlayer in ipairs(players) do
		if getElementData(thePlayer, "auth") == true then
			local name, skin, money = getElementData(thePlayer, "name"), getElementData(thePlayer, "skin"), getPlayerMoney(thePlayer, "money")
			dbExec(database, "UPDATE accounts SET skin = '"..tonumber(skin).."' WHERE name = '"..name.."';")
			dbExec(database, "UPDATE accounts SET money = '"..tonumber(money).."' WHERE name = '"..name.."';")
			local inventory = exports.inv_system:getPlayerInventory(name)
			dbExec(database, "UPDATE accounts SET inventory = '"..toJSON(inventory).."' WHERE name = '"..name.."';")
			local savedPlayers = savedPlayers + 1
			if playerCount == savedPlayers then
				--outputChatBox("Автосохранение: Сохранено "..savedPlayers.." аккаунтов из "..getPlayerCount()..".", getRootElement(), 0, 255, 0)
				for _, playerSource in ipairs(players) do
					exports.ns:sendNotification(playerSource, "info", "Автосохранение: Сохранено "..savedPlayers.." аккаунтов из "..getPlayerCount()..".")
				end
			end
		end
	end
end
setTimer(autosave, 300000, 0)