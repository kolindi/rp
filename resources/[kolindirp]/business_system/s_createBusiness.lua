database = call(getResourceFromName("db"), "db")
function createBusiness(x, y, z, intId, intx, inty, intz, busiType)
	if hasObjectPermissionTo(source, "command.createBusi") then
		if busiType == "office" then
			name = "Офис"
		elseif busiType == "magaz" then
			name = "Магазин"
		elseif busiType == "zavod" then
			name = "Производство"
		end
		outputChatBox(busiType)
		local query = dbExec(database, "INSERT INTO business (type, name, x, y, z, int_id, int_x, int_y, int_z) VALUES ('"..busiType.."', '"..name.."', '"..tonumber(x).."', '"..tonumber(y).."', '"..tonumber(z).."', '"..tonumber(intId).."', '"..tonumber(intx).."', '"..tonumber(inty).."', '"..tonumber(intz).."');")
		if query then
			exports.ns:sendNotification(source, "success", "Бизнес типа: "..name.." успешно создан!")
		end
	end
end
addEventHandler("onAdminCreateBusiness", getRootElement(), createBusiness)