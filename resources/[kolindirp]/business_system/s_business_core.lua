database = call(getResourceFromName("db"), "getDb")
function businessInit()
	local query = dbQuery(database, "SELECT * FROM business;")
	local result = dbPoll(query, -1)
	for _, row in ipairs(result) do
		business = createElement("business", row['id'])
		businessPickup = createPickup(row['x'], row['y'], row['z'], 3, 1318, 1)
		--businessExit = createPickup(row['int_x'], row['int_y'], row['int_z'], 3, 1318, 1)
		--setElementInterior(businessExit, row['int_id'])
		setElementData(businessPickup, "business", business)
		setElementData(business, "int_x", row['int_x'])
		setElementData(business, "int_y", row['int_y'])
		setElementData(business, "int_z", row['int_z'])
		setElementData(business, "int_id", row['int_id'])
		setElementData(business, "isOpen", tonumber(row['isOpen']))
		setElementData(business, "owner", tonumber(row['owner']))
		setElementData(business, "price", tonumber(row['price']))
		setElementData(business, "name", row['name'])
		setElementData(business, "busi", row['business'])
		if getElementData(business, "owner") ~= 1 then
			if getElementData(business, "busi") == "bank" then
				setElementData(business, "bankInputPer", tonumber(row['bankInputPerc']))
				setElementData(business, "bankWithPer", tonumber(row['bankWithPerc']))
			end
			if getElementData(business, "isOpen") == 1 then
				isOpen = "Открыто"
			else
				isOpen = "Закрыто"
			end
			local query = dbQuery(database, "SELECT name FROM accounts WHERE id = '"..getElementData(business, "owner").."';")
			local result = dbPoll(query, -1)
			for _, row in ipairs(result) do
				ownerName = row['name']
				setElementData(business, "ownerName", ownerName)
			end
			setElementData(businessPickup, "text", ""..row['name'].."\n"..isOpen.."\nВладелец: "..ownerName.."")
			local query = dbQuery(database, "SELECT * FROM busiObj WHERE busiId = '"..row['id'].."';")
			local result = dbPoll(query, -1)
			for _, row in ipairs(result) do
				if row['type'] == "obj" then
					obj = createObject(row['objId'], row['x'], row['y'], row['z'], row['rx'], row['ry'], row['rz'])
					setElementInterior(obj, getElementData(business, "int_id"))
					setElementDimension(obj, getElementID(business) + 50)
				elseif row['type'] == "npc" then
					npc = createPed(row['objId'], row['x'], row['y'], row['z'], row['rz'], false)
					setElementData(npc, "unDead", true)
					setElementInterior(npc, getElementData(business, "int_id"))
					setElementDimension(npc, getElementID(business) + 50)
					setElementData(npc, "business", business)
				end
			end
		elseif getElementData(business, "owner") == 1 then
			price = row['price']
			if row['type'] == "office" then
				name = "Офис"
			elseif row['type'] == "magaz" then
				name = "Магазин"
			elseif row['type'] == "zavod" then
				name = "Производство"
			end
			setElementData(businessPickup, "text", ""..name.."\nПродаётся: "..price.."\nНомер бизнеса: "..row['id'].."")
		end
	end
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), businessInit)

function businessPickup(playerSource)
	if source == businessPickup then
		business = getElementData(businessPickup, "business")
		if getElementData(business, "isOpen") == 1 or getElementData(business, "owner") == getElementData(playerSource, "id") then
			x, y, z = getElementPosition(playerSource)
			setElementData(playerSource, "exit_x", x)
			setElementData(playerSource, "exit_y", y)
			setElementData(playerSource, "exit_z", z)
			setElementData(playerSource, "inHouse", 1)
			setElementInterior(playerSource, getElementData(business, "int_id"))
			setElementPosition(playerSource, getElementData(business, "int_x"), getElementData(business, "int_y"), getElementData(business, "int_z"))
			setElementDimension(playerSource, getElementID(business) + 50)
		else
			exports.ns:sendNotification(playerSource, "info", "Закрыто...")
		end
	end
end
addEventHandler("onPickupUse", getRootElement(), businessPickup)

function businessExit()
	setElementInterior(source, 0)
	setElementDimension(source, 0)
	setElementPosition(source, getElementData(source, "exit_x") + 0.5, getElementData(source, "exit_y"), getElementData(source, "exit_z"))
	setElementData(source, "inHouse", 0)
end
addEventHandler("onClientExitHouseClick", getRootElement(), businessExit)

function npcTalk(targetElem)
	if targetElem == npc then
		hasAccount = false
		if getElementData(business, "busi") == "bank" then
			local query = dbQuery(database, "SELECT * FROM bankAccounts WHERE owner = '"..getElementData(source, "id").."';")
			local result = dbPoll(query, -1)
			bankId = tonumber(getElementID(business))
			for _, row in ipairs(result) do
				local accountBankId = tonumber(row['bankId'])
				if accountBankId == bankId then
					hasAccount = true
				end
			end
			if hasAccount ~= true then
				hasAccount = false
			end
			triggerClientEvent(source, "onPlayerTalkOnNPC", source, npc, hasAccount)
		end
	end
end
addEventHandler("onPlayerTarget", getRootElement(), npcTalk)

function createBankAccount(bank)
	local time = getRealTime()
	local timestamp = time.timestamp
	local query = dbExec(database, "INSERT INTO bankAccounts (owner, bankId, createTime) VALUES ('"..getElementData(source, "id").."', '"..tonumber(getElementID(bank)).."', '"..tonumber(timestamp).."');")
end
addEventHandler("onClientCreateBankAccount", getRootElement(), createBankAccount)

function getBankBalance(bankId)
	local query = dbQuery(database, "SELECT `id`, `balance` FROM bankAccounts WHERE owner = '"..getElementData(source, "id").."' AND bankId = '"..bankId.."';")
	local result = dbPoll(query, -1)
	for _, row in ipairs(result) do
		local balance = tonumber(row['balance'])
		local id = tonumber(row['id'])
		triggerClientEvent(source, "successGetBankBalance", source, balance, id)
	end
end
addEventHandler("onClientCheckBankBalance", getRootElement(), getBankBalance)

function inputMoneyBank(input, bankId, percent)
	local ownerId = getElementData(source, "id")
	local query = dbQuery(database, "SELECT balance FROM bankAccounts WHERE owner = '"..ownerId.."' AND bankId = '"..bankId.."';")
	local result = dbPoll(query, -1)
	for _, row in ipairs(result) do
		inputTotal = tonumber(row['balance']) + input
	end
	local query = dbExec(database, "UPDATE bankAccounts SET balance = '"..inputTotal.."' WHERE owner = '"..ownerId.."' AND bankId = '"..bankId.."';")
	setPlayerMoney(source, getPlayerMoney(source) - input)
	local query = dbQuery(database, "SELECT balance FROM business WHERE id = '"..bankId.."';")
	local result = dbPoll(query, -1)
	for _, row in ipairs(result) do
		local bankBalance = tonumber(row['balance'])
		local bankBalance = bankBalance + percent + input
		local query = dbExec(database, "UPDATE business SET balance = '"..bankBalance.."' WHERE id = '"..bankId.."';")
	end
end
addEventHandler("onClientInputMoneyBank", getRootElement(), inputMoneyBank)

function withdrawMoneyBank(total, bankid, percent)
	local ownerId = getElementData(source, "id")
	local query = dbQuery(database, "SELECT balance FROM bankAccounts WHERE owner = '"..ownerId.."' AND bankId = '"..bankId.."';")
	local result = dbPoll(query, -1)
	for _, row in ipairs(result) do
		totalTotal = tonumber(row['balance']) - total
		if totalTotal < 1 then
			triggerClientEvent(source, "noMoneyOnBalance", source)
		else
			local query = dbQuery(database, "SELECT balance FROM business WHERE id = '"..bankId.."';")
			local result = dbPoll(query, -1)
			for _, row in ipairs(result) do
				local bankBalance = tonumber(row['balance'])
				local bankBalance = bankBalance + percent - total
				if bankBalance < 1 then
					triggerClientEvent(source, "noMoneyInBank", source)
				else
					local query = dbExec(database, "UPDATE business SET balance = '"..bankBalance.."' WHERE id = '"..bankId.."';")
					local query = dbExec(database, "UPDATE bankAccounts SET balance = '"..totalTotal.."' WHERE owner = '"..ownerId.."' AND bankId = '"..bankId.."';")
					setPlayerMoney(source, getPlayerMoney(source) + total)
					triggerClientEvent(source, "successWithdrawBank", source)
				end
			end
		end
	end
end
addEventHandler("onClientWithdrawMoneyBank", getRootElement(), withdrawMoneyBank)