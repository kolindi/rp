DS = exports.dialog_system
DGS = exports.dgs
BusiGUI = {}
bankWindowOpen = false
function npcBank(npc, hasAccount)
	if bankWindowOpen == false then
		bankWindowOpen = true
		bank = getElementData(npc, "business")
		bankInputPer = getElementData(bank, "bankInputPer")
		bankWithPer = getElementData(bank, "bankWithPer")
		bankId = tonumber(getElementID(bank))
		--business = getElementData(businessElem, "busi")
		showCursor(true)
		BusiGUI.window = DGS:dgsDxCreateWindow(0.30, 0.25, 0.50, 0.45, getElementData(bank, "name"), true)
		BusiGUI.bankText = DGS:dgsDxCreateLabel(0.01, 0.05, 0.9, 0.1, "-Добро пожаловать в "..getElementData(bank, "name")..", чем я могу помочь?", true, BusiGUI.window)
		BusiGUI.bankInputPerLabel = DGS:dgsDxCreateLabel(0.7, 0.025, 0.2, 0.1, "Процент с ввода Колиндов: "..bankInputPer.."%", true, BusiGUI.window)
		BusiGUI.bankWithPerLabel = DGS:dgsDxCreateLabel(0.7, 0.065, 0.2, 0.1, "Процент с вывода Колиндов: "..bankWithPer.."%", true, BusiGUI.window)
		if hasAccount == true then
			BusiGUI.checkBankBalance = DGS:dgsDxCreateButton(0, 0.2, 1, 0.1, "Информация о счёте", true, BusiGUI.window)
			BusiGUI.inputMoneyBank = DGS:dgsDxCreateButton(0, 0.35, 1, 0.1, "Внести деньги", true, BusiGUI.window)
			BusiGUI.withdrawMoneyBank = DGS:dgsDxCreateButton(0, 0.50, 1, 0.1, "Вывести деньги", true, BusiGUI.window)
			BusiGUI.creditBank = DGS:dgsDxCreateButton(0, 0.65, 1, 0.1, "Оформить кредит", true, BusiGUI.window)
		elseif hasAccount == false then
			BusiGUI.createBankAccount = DGS:dgsDxCreateButton(0, 0.2, 1, 0.1, "Создать банковский счет в этом банке", true, BusiGUI.window)
		end
	end
end
addEventHandler("onPlayerTalkOnNPC", getRootElement(), npcBank)

function busiButtonClicked(button, state)
	if state == "up" then
		if source == BusiGUI.createBankAccount then
			destroyElement(BusiGUI.createBankAccount)
			triggerServerEvent("onClientCreateBankAccount", localPlayer, bank)
			DGS:dgsDxGUISetText(BusiGUI.bankText, "-Мы рады, что вы выбрали наш банк. Счет успешно создан.")
		elseif source == BusiGUI.checkBankBalance then
			--if BusiGUI.inputMoneyBankButton or BusiGUI.withdrawMoneyBankButton then
				destroyElement(BusiGUI.withdrawMoneyBankButton)
				destroyElement(BusiGUI.withdrawMoneyBankEdit)
				destroyElement(BusiGUI.inputMoneyBankButton)
				destroyElement(BusiGUI.inputMoneyBankEdit)
			--end
			triggerServerEvent("onClientCheckBankBalance", localPlayer, bankId)
		elseif source == BusiGUI.inputMoneyBank then
			--if BusiGUI.withdrawMoneyBankButton then
				destroyElement(BusiGUI.withdrawMoneyBankButton)
				destroyElement(BusiGUI.withdrawMoneyBankEdit)
			--end
			DGS:dgsDxGUISetText(BusiGUI.bankText, "-Какую сумму вы хотите внести?")
			BusiGUI.inputMoneyBankEdit = DGS:dgsDxCreateEdit(0.1, 0.76, 0.8, 0.1, "", true, BusiGUI.window)
			BusiGUI.inputMoneyBankButton = DGS:dgsDxCreateButton(0.3, 0.87, 0.4, 0.1, "Внести", true, BusiGUI.window)
		elseif source == BusiGUI.withdrawMoneyBank then
			--if BusiGUI.inputMoneyBankButton then
				destroyElement(BusiGUI.inputMoneyBankButton)
				destroyElement(BusiGUI.inputMoneyBankEdit)
			--end
			DGS:dgsDxGUISetText(BusiGUI.bankText, "-Какую сумму вы хотите вывести?")
			BusiGUI.withdrawMoneyBankEdit = DGS:dgsDxCreateEdit(0.1, 0.76, 0.8, 0.1, "", true, BusiGUI.window)
			BusiGUI.withdrawMoneyBankButton = DGS:dgsDxCreateButton(0.3, 0.87, 0.4, 0.1, "Вывести", true, BusiGUI.window)
		elseif source == BusiGUI.inputMoneyBankButton then
			money = tonumber(getPlayerMoney(localPlayer))
			input = tonumber(DGS:dgsDxGUIGetText(BusiGUI.inputMoneyBankEdit))
			if money - input < 1 then
				exports.ns:sendCNotification("abort", "У вас недостаточно денег!")
			elseif money - input > 1 then
				percent = input*bankInputPer/100
				input = input - percent
				triggerServerEvent("onClientInputMoneyBank", localPlayer, input, bankId, percent)
				DGS:dgsDxGUISetText(BusiGUI.bankText, "-С учётом "..bankInputPer.."% комиссии ваш счёт пополнен на "..input.." Колиндов")
			end
		elseif source == BusiGUI.withdrawMoneyBankButton then
			total = tonumber(DGS:dgsDxGUIGetText(BusiGUI.withdrawMoneyBankEdit))
			if total then
				percent = total*bankWithPer/100
				total = total - percent
				triggerServerEvent("onClientWithdrawMoneyBank", localPlayer, total, bankId, percent)
			end
			function noMoneyOnBalance()
				DGS:dgsDxGUISetText(BusiGUI.bankText, "-На вашем счёте недостаточно денег.")
			end
			addEventHandler("noMoneyOnBalance", getRootElement(), noMoneyOnBalance)
			function noMoneyInBank()
				DGS:dgsDxGUISetText(BusiGUI.bankText, "-В данный момент в банке недостаточно денег для вывода такой суммы.")
				--DGS:dgsSetPosition(BusiGUI.bankText, )
			end
			addEventHandler("noMoneyInBank", getRootElement(), noMoneyInBank)
			function successWithdrawBank()
				DGS:dgsDxGUISetText(BusiGUI.bankText, "-С учётом "..bankWithPer.."% комиссии вы сняли со счёта "..total.." Колиндов")
			end
			addEventHandler("successWithdrawBank", getRootElement(), successWithdrawBank)
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), busiButtonClicked)

function setBalanceText(balance, id)
	DGS:dgsDxGUISetText(BusiGUI.bankText, "-Номер счёта: "..id.."\n-На счёте: "..balance.." Колиндов")
end
addEventHandler("successGetBankBalance", getRootElement(), setBalanceText)

function destroyGeneralElements()
	destroyElement(BusiGUI.checkBankBalance)
end

function busiWindowClosed()
	if source == BusiGUI.window then
		showCursor(false)
		bankWindowOpen = false
	end
end
addEventHandler("onClientDgsDxWindowClose", getRootElement(), busiWindowClosed)