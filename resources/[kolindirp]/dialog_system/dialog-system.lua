DGS = exports.dgs
function createDialog(text, answer1, answer2, answer3, answer4, action1, action2, action3, action4)
	dialogWindow = DGS:dgsDxCreateWindow(0.31, 0.69, 0.42, 0.24, "Диалог", true)
	dialogWindowSizable = DGS:dgsDxWindowSetSizable(dialogWindow, false)
	dialogText = DGS:dgsDxCreateEdit(0.02, 0.10, 0.96, 0.43, text, true, dialogWindow)
	textReadOnly = DGS:dgsDxEditSetReadOnly(dialogText, true)
	cursor = showCursor(true)
	playerId = getLocalPlayer()
	function dialogAnswer(button, state)
		if state == "up" then
			if (source == dialogAnswer1) then
				destroyElement(dialogWindow)
				showCursor(false)
				dialogAction(action1)
			elseif (source == dialogAnswer2) then
				destroyElement(dialogWindow)
				showCursor(false)
				dialogAction(action2)
			elseif (source == dialogAnswer3) then
				destroyElement(dialogWindow)
				showCursor(false)
				dialogAction(action3)
			elseif (source == dialogAnswer4) then
				destroyElement(dialogWindow)
				showCursor(false)
				dialogAction(action4)
			end
		end
	end
	addEventHandler("onClientDgsDxMouseClick", root, dialogAnswer)
	if (answer1 ~= "") and (answer2 == "") then
		dialogAnswer1 = DGS:dgsDxCreateButton(0.21, 0.58, 0.62, 0.38, answer1, true, dialogWindow)
		return dialogWindow, dialogWindowSizable, dialogText, dialogAnswer1, cursor, textReadOnly
	elseif (answer2 ~= "") and (answer3 == "") then
		dialogAnswer1 = DGS:dgsDxCreateButton(0.03, 0.58, 0.44, 0.38, answer1, true, dialogWindow)
		dialogAnswer2 = DGS:dgsDxCreateButton(0.49, 0.58, 0.44, 0.38, answer2, true, dialogWindow)
		return dialogWindow, dialogWindowSizable, dialogText, dialogAnswer1, dialogAnswer2, cursor, textReadOnly
	elseif (answer3 ~= "") and (answer4 == "") then
		dialogAnswer1 = DGS:dgsDxCreateButton(0.04, 0.61, 0.28, 0.35, answer1, true, dialogWindow)
		dialogAnswer2 = DGS:dgsDxCreateButton(0.36, 0.61, 0.28, 0.35, answer2, true, dialogWindow)
		dialogAnswer3 = DGS:dgsDxCreateButton(0.69, 0.61, 0.28, 0.35, answer3, true, dialogWindow)
		return dialogWindow, dialogWindowSizable, dialogText, dialogAnswer1, dialogAnswer2, dialogAnswer3, cursor, textReadOnly
	elseif (answer4 ~= "") then
		dialogAnswer1 = DGS:dgsDxCreateButton(0.02, 0.57, 0.46, 0.16, answer1, true, dialogWindow)
		dialogAnswer2 = DGS:dgsDxCreateButton(0.02, 0.76, 0.46, 0.16, answer2, true, dialogWindow)
		dialogAnswer3 = DGS:dgsDxCreateButton(0.50, 0.57, 0.46, 0.16, answer3, true, dialogWindow)
		dialogAnswer4 = DGS:dgsDxCreateButton(0.50, 0.76, 0.46, 0.16, answer4, true, dialogWindow)
		return dialogWindow, dialogWindowSizable, dialogText, dialogAnswer1, dialogAnswer2, dialogAnswer3, dialogAnswer4, cursor, textReadOnly
	else
		outputDebugString("Ошибка создания диалога!", 255, 0, 0)
	end
end

function dialogAction(action)
	loadstring(action)()
end

addCommandHandler("dialogTest",
	function()
		createDialog("Текст вашего диалога", "Ответ 1", "Ответ 2", "Ответ 3", "Ответ 4", "", "", "", "")
	end
)

function closeDialogWindow()
	if source == dialogWindow then
		showCursor(false)
	end
end
addEventHandler("onClientDgsDxWindowClose", getRootElement(), closeDialogWindow)

--[[function dialogSysStarted(res)
	outputDebugString("Система диалогов запущена!")
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), dialogSysStarted)]]