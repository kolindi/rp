function getPlayerByID(id)
	local players = getElementsByType("player")
	local id = tonumber(id)
	for key, thePlayer in ipairs(players) do
		if getElementData(thePlayer, "id") == id then
			return thePlayer
		end
	end
end

function getVehicleByID(id)
	--outputChatBox(id)
	local vehicles = getElementsByType("vehicle")
	for key, vehicle in ipairs(vehicles) do
		if getElementData(vehicle, "id") == id then
			--outputChatBox("нашло")
			return vehicle
		end
	end
end