onSpawnHouseExit = false
function shExitCommand(x, y, z)
	x, y, z = x, y, z
	function shExit()
		if onSpawnHouseExit == false then
			setElementInterior(getLocalPlayer(), 0)
			setElementDimension(getLocalPlayer(), 0)
			setElementPosition(getLocalPlayer(), x, y, z)
			onSpawnHouseExit = true
		end
	end
	addCommandHandler("hExit", shExit)
end
--addCommandHandler("hExit", shExit)
addEventHandler("onPlayerHouseSpawn", getRootElement(), shExitCommand)