database = call(getResourceFromName("db"), "db")
function playerSpawn()
	if getElementData(source, "house_id") == 0 then
		setElementPosition(source, 1720.3251953125, -1741.166015625, 13.546875)
	else
		local id = getElementData(source, "house_id")
		local query = dbQuery(database, "SELECT id, int_id, x, y, z, int_x, int_y, int_z FROM house WHERE id = '"..id.."';")
		local result = dbPoll(query, -1)
		for rowName, rowData in ipairs(result) do
			setElementDimension(source, rowData['id'] + 100)
			setElementInterior(source, rowData['int_id'])
			setElementPosition(source, rowData['int_x'], rowData['int_y'], rowData['int_z'])
			x, y, z = rowData['x'], rowData['y'], rowData['z']
		end
		triggerClientEvent("onPlayerHouseSpawn", source, x, y, z)
	end
end
addEventHandler("onPlayerAuth", getRootElement(), playerSpawn)