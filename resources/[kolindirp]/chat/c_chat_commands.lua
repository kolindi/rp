function cursor(commandName)
	if isCursorShowing() == true then
		showCursor(false)
	elseif isCursorShowing() == false then
		showCursor(true)
	end
end
addCommandHandler("cursor", cursor)
bindKey("F6", "up", cursor)