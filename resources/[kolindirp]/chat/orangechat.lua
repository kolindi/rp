function rangedMessage(thePlayer, message, chatRadius) -- This is a custom function should you wish to use it in your script which sends a message to players within a certain distance of a player.
		local posX, posY, posZ = getElementPosition( thePlayer ) -- find the player's position
		local chatSphere = createColSphere( posX, posY, posZ, chatRadius ) -- make a collision sphere at that position with the radius specified by chatRadius
		local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" ) -- get a table all player elements inside the colSphere
		destroyElement( chatSphere ) -- delete the redundant colSphere
		for index, nearbyPlayer in ipairs( nearbyPlayers ) do -- deliver the message to each player in that table
			outputChatBox( message, nearbyPlayer, 200, 200, 200 )
		end
	end
	
function rangedChat( message, messageType)
		local playerName = getClientName ( source ) -- get the player's name
		local posX, posY, posZ = getElementPosition( source )
		local global = string.find(message, "#", 1, true)
		local cancel = string.find(message, ".", 1, true)
		local shouted = string.find(message, "!", 1, true) -- check if it has a ! in it
		local asked = string.find(message, "?", 1, true) -- check if it has a ? in it
		local ooc, ooc1 = string.find(message, ")", 1, true), string.find(message, "(", 1, true) -- check if it has a ) in it and check if the first item in the table has a ( in it
		local happy, happy1, happy2 = string.find(message, ")", 1, true), string.find(message, ": )", 1, true), string.find(message, "=)", 1, true)
		local unhappy, unhappy1, unhappy2 = string.find(message, ":(", 1, true), string.find(message, ": (", 1, true), string.find(message, "=(", 1, true)
		local crying, crying1, crying2 = string.find(message, ":'(", 1, true), string.find(message, ":' (", 1, true), string.find(message, "='(", 1, true)
		local wink, wink1 = string.find(message, ";)", 1, true), string.find(message, "; )", 1, true)
		local delight, delight1, delight2 = string.find(message, ":)", 1, true), string.find(message, ": D", 1, true), string.find(message, "=D", 1, true)
		local horror, horror1, horror2 = string.find(message, "D:", 1, true), string.find(message, "D :", 1, true), string.find(message, "D=", 1, true)
		--local angry, angry1 = string.find(message, "):<", 1, true), string.find(message, ">:(", 1, true)
		--local evil = string.find(message, ">:D", 1, true)
		--local fuming = string.find(message, ":@", 1, true)
		if messageType == 0 then -- If it's normal chat (i.e. not PM or team)
			if global then
				local players = getElementsByType("player")
				message = string.gsub(message, "#", "")
				for index, player in ipairs ( players ) do
					outputChatBox( "[GLOBAL] ".. playerName.. ": " ..message, player, 150, 150, 255)
				end
			elseif cancel then
				local chatRadius = 10
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius ) 
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" ) 
				destroyElement( chatSphere ) 
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." говорит: " ..message, nearbyPlayer, 200, 200, 200 )
				end
			elseif asked then -- does message end with a '?' ? 
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." спрашивает: " ..message, nearbyPlayer, 200, 200, 200 )
				end
			elseif ooc and ooc1 then -- is the message in brackets?
				message = string.gsub(message, "%(", "") -- delete the (
				message = string.gsub(message, "%)", "") -- and the )
				local chatRadius = 10
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( "( Нон-РП ) " ..playerName.. " говорит: " ..message.. "", nearbyPlayer, 211, 211, 211 )
				end
			elseif shouted then -- if it did have a ! in it then
				local chatRadius = 20 -- the message will be sent to players up to 20 metres away from the player
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius ) -- make a collision sphere at that position with the radius specified by chatRadius
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" ) -- get a table of all player elements inside the colSphere
				destroyElement( chatSphere ) -- delete the redundant colSphere
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do -- for each of the nearby players, do this:
					outputChatBox( playerName.." кричит: " ..message, nearbyPlayer, 200, 200, 200 ) -- table.concat makes the table into a string, with each item seperated by a space
				end
			elseif happy or happy1 or happy2 then
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." улыбается", nearbyPlayer, 255, 165, 0 )
				end
			elseif unhappy or unhappy1 or unhappy2 then
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." растроился", nearbyPlayer, 255, 165, 0 )
				end
			elseif crying or crying1 or crying2 then
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." кричит", nearbyPlayer, 200, 200, 200 )
				end
			elseif wink or wink1 then
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." подмигивает", nearbyPlayer, 255, 165, 0 )
				end
			elseif delight or delight1 or delight2 then
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." смеется", nearbyPlayer, 255, 165, 0 )
				end
			elseif horror or horror1 or horror2 then
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." в ужасе", nearbyPlayer, 255, 165, 0 )
				end
			elseif angry or angry1 then
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." сердится", nearbyPlayer, 255, 165, 0 )
				end
			elseif evil then
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." злобно смеется", nearbyPlayer, 255, 165, 0 )
				end
			elseif fuming then
				local chatRadius = 10 
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius )
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" )
				destroyElement( chatSphere )
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." бомбит", nearbyPlayer, 255, 165, 0 )
				end
			else -- i.e. it's normal
				local chatRadius = 10
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius ) 
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" ) 
				destroyElement( chatSphere ) 
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( playerName.." говорит: " ..message, nearbyPlayer, 200, 200, 200 )
				end
			end
		elseif messageType == 1 then -- if it's /me
			if global then -- if there is a # infront of the text
				local players = getElementsByType("player") -- find all the players in the server
				message = string.gsub(message, "#", "")
				for index, player in ipairs ( players ) do
					outputChatBox( "[PUBLIC] * ".. playerName.. " " ..message, player, 150, 150, 255)
				end
			else
				local chatRadius = 10
				local chatSphere = createColSphere( posX, posY, posZ, chatRadius ) 
				local nearbyPlayers = getElementsWithinColShape( chatSphere, "player" ) 
				destroyElement( chatSphere ) 
				for index, nearbyPlayer in ipairs( nearbyPlayers ) do 
					outputChatBox( "* " ..playerName.." " ..message, nearbyPlayer, 200, 200, 200 )
				end
			end
		elseif messageType == 2 then -- If it's team chat
			local playerTeam = getPlayerTeam(source) -- Find what team the player is
			local teamMates = getPlayersInTeam( playerTeam ) -- Find his team mates
			for index, teamMate in ipairs( teamMates ) do -- For each of his team mates, do this:
				if shouted then
					outputChatBox( "[RADIO] " ..playerName.. " shouts: " ..message, teamMate, 150, 255, 150 )
				elseif asked then
					outputChatBox( "[RADIO] " ..playerName.. " asks: " ..message, teamMate, 150, 255, 150 )
				elseif ooc and ooc1 then
					message = string.gsub(message, "%(", "") -- find and delete all the brackets
					message = string.gsub(message, "%)", "") -- '' ''
					outputChatBox( "(([RADIO] " ..playerName.. " says (OOC): " ..message.. "))", teamMate, 150, 255, 150 )
				else
					outputChatBox( "[RADIO] " ..playerName.. " says: " ..message, teamMate, 150, 255, 150 )
				end
			end
		end
	end

function publicChat(thePlayer, commandName, ...)
		local players = getElementsByType("player")
		local playerName = getClientName ( thePlayer )
		local chatContent = {...}
		for index, player in ipairs ( players ) do
			outputChatBox( "[PUBLIC] ".. playerName.. ": " ..table.concat ( chatContent, " "), player, 150, 150, 255)
		end
	end
addCommandHandler( "p", publicChat ) -- /p [text]

--Stop MTA's built-in chat 
function blockChatMessage(message, messageType)
		cancelEvent()
	end
addEventHandler( "onPlayerChat", getRootElement(), blockChatMessage)
addEventHandler( "onPlayerChat", getRootElement(), rangedChat )