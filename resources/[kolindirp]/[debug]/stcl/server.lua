--#####################################################################################
--# ВАЖНО! ЧИТАТЬ ОБЯЗАТЕЛЬНО!
--# Не забудь подписаться на YouTube канал - Tameronik Show :3
--# Там ты найдешь различные обзоры на маппинг, объекты, ресурсы для сервера MTA.
--# Ссылка на YouTube канал - https://www.youtube.com/user/8VaaeBxqZxA1
--# ГРУППА ВКОНТАКТЕ - vk.com/tameronik (Новости/Стримы/Розыгрыши и многое другое!)
--# ВНИМАНИЕ! ДАННЫЙ РЕСУРС НЕ ЯВЛЯЕТСЯ МОИМ! Я ЕГО НАШЁЛ НА ПРОСТОРАХ ИНТЕРНЕТА!
--# АВТОР: Неизвестен! 
--#####################################################################################

local playerTimers = {}
function sendStats(player)
	if isElement(player) then
		local columns, rows = getPerformanceStats("Lua timing")
		triggerClientEvent(player, "receiveServerStat", player, columns, rows)
		playerTimers[player] = setTimer(sendStats, 1000, 1, player)
	end
end

addEvent("getServerStat", true)
addEventHandler("getServerStat", root, function()
	sendStats(client)
end)

addEvent("destroyServerStat", true)
addEventHandler("destroyServerStat", root, function()
	if isTimer(playerTimers[client]) then
		killTimer(playerTimers[client])
		playerTimers[client] = nil
	end
end)