i18n = exports.i18n

function lspdinit()
	lspd_enter = createPickup(1555.244140625, -1675.5830078125, 16.1953125, 3, 1318, 1)
	setElementData(lspd_enter, "text", "LSPD")
	lspd_exit = createPickup(246.8671875, 62.5458984375, 1003.64062553125, 3, 1318, 1)
	setElementInterior(lspd_exit, 6)
	lspd_downEnter = createPickup(1568.587890625, -1690.0283203125, 6.21875, 3, 1318, 1)
	lspd_downExit = createPickup (242.505859375, 66.3056640625, 1003.640625, 3, 1318, 1)
	setElementInterior(lspd_downExit, 6)
	lspd_blip = createBlip(1555.244140625, -1675.5830078125, 16.1953125, 30, 2, 255, 0, 0, 255, 0, 150)

	lspd_policeEnterMarker = createMarker(246.7255859375, 71.8466796875, 1002.6406258955, "cylinder", 2, 0, 0, 255, 0)
	setElementInterior(lspd_policeEnterMarker, 6)
	setElementData(lspd_policeEnterMarker, "text", "LSPD")
	lspd_policeExitMarker = createMarker(246.7, 74.1201171875, 1002.6406258955, "cylinder", 2, 0, 0, 255, 0)
	setElementInterior(lspd_policeExitMarker, 6)
	lspd_policeEnterDoor = createObject(2930, 245.5, 72.4697265625, 1005.2, 0, 0, 90)
	setElementInterior(lspd_policeEnterDoor, 6)

	lspd_equipmentMarker = createMarker(254.9462890625, 76.927734375, 1002.64062558955, "cylinder", 1.5, 255, 0, 0, 100)
	setElementInterior(lspd_equipmentMarker, 6)
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), lspdinit)

function lspdPickupHit(playerSource)
	if getElementType(playerSource) == "player" then
		if source == lspd_enter then
			setElementInterior(playerSource, 6)
			setElementPosition(playerSource, 246.4510, 65.5860,	1003.6410)
		elseif source == lspd_exit then
			setElementInterior(playerSource, 0)
			setElementPosition(playerSource, 1552.0703125, -1675.720703125, 16.129108428955)
		elseif source == lspd_downEnter then
			setElementInterior(playerSource, 6)
			setElementPosition(playerSource, 245.1796875, 66.4189453125, 1003.640625)
		elseif source == lspd_downExit then
			setElementInterior(playerSource, 0)
			setElementPosition(playerSource, 1568.6240234375, -1693.4267578125, 5.890625)
		end
	end
end
addEventHandler("onPickupHit", getRootElement(), lspdPickupHit)

function policeDoorOpen(hitElement)
	if source == lspd_policeEnterMarker or source == lspd_policeExitMarker and getElementType(source) == "player" then
		if getElementData(hitElement, "org") == "Police" then
			moveObject(lspd_policeEnterDoor, 2000, 244, 72.4697265625, 1005.2, 0, 0, 0)
		else
			exports.ns:sendNotification(source, "abort", "Дверь не открылась...")
		end
	end
end	
addEventHandler("onMarkerHit", getRootElement(), policeDoorOpen)

function policeDoorClose(hitElement)
	if source == lspd_policeExitMarker or source == lspd_policeEnterMarker then
		moveObject(lspd_policeEnterDoor, 2000, 245.5, 72.4697265625, 1005.2)
	end
end
addEventHandler("onMarkerLeave", getRootElement(), policeDoorClose)

function equipmentMarkerHit(hitElement)
	if source == lspd_equipmentMarker and getElementData(hitElement, "org") == "Police" then
		triggerClientEvent("onPlayerEquipmentMarkerHit", hitElement)
	end
end
addEventHandler("onMarkerHit", getRootElement(), equipmentMarkerHit)