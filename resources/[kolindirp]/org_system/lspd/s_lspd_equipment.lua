function giveEquip(object)
	if object == "cuff" then
		if getElementData(source, "cuffs") == 4 then
			exports.ns:sendNotification(source, "notif", "Больше не поместиться.")
		else
			setElementData(source, "cuffs", 4)
		end
	elseif object == "pistol" then
		if getElementData(source, "pistol") == 1 then
			setWeaponAmmo(source, 22, 48)
			exports.ns:sendNotification(source, "notif", "Вы пополнили свои запасы магазинов.")
		else
			giveWeapon(source, 22, 68)
			setElementData(source, "pistol", 1)
			exports.ns:sendNotification(source, "notif", "У вас уже есть пистолет и патроны к нему.")
		end
	elseif object == "baton" then
		if getElementData(source, "baton") == 1 then
			exports.ns:sendNotification(source, "notif", "У вас уже есть дубинка.")
		else
			giveWeapon(source, 3)
			setElementData(source, "baton", 1)
		end
	end
end
addEventHandler("onClientPoliceEquipmentClick", getRootElement(), giveEquip)