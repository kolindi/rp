DGS = exports.dgs
equipMenuOpen = false
function equipmentWindow()
	if getElementData(localPlayer, "org") == "Police" then
		showCursor(true)
		equipWindow = DGS:dgsDxCreateWindow(0.40, 0.50, 0.30, 0.40, "Взять снаряжение", true)
		cuffButton = DGS:dgsDxCreateButton(0, 0, 0.40, 0.10, "Наручники", true, equipWindow)
		cuffImg = DGS:dgsDxCreateImage(0.40, 0, 0.10, 0.10, ":inv_system/img/handcuffs.png", true, equipWindow)
		batonButton = DGS:dgsDxCreateButton(0.60, 0, 0.40, 0.10, "Дубинка", true, equipWindow)
		batonImg = DGS:dgsDxCreateImage(0.50, 0, 0.10, 0.10, ":inv_system/img/baton.png", true, equipWindow)
		tazerButton = DGS:dgsDxCreateButton(0, 0.10, 0.40, 0.10, "Электрошокер", true, equipWindow)
		tazerImg = DGS:dgsDxCreateImage(0.40, 0.10, 0.10, 0.10, ":inv_system/img/tazer.png", true, equipWindow)
		pistolButton = DGS:dgsDxCreateButton(0.60, 0.10, 0.40, 0.10, "Пистолет", true, equipWindow)
		pistolImg = DGS:dgsDxCreateImage(0.50, 0.10, 0.10, 0.10, ":inv_system/img/pistol.png", true, equipWindow)
	else
		exports.ns:sendCNotification("info", "У вас нет доступа сюда.")
	end
end
addEventHandler("onPlayerEquipmentMarkerHit", localPlayer, equipmentWindow)

function equipmentWindowClose()
	if source == equipWindow then
		showCursor(false)
		equipMenuOpen = false
	end
end
addEventHandler("onClientDgsDxWindowClose", root, equipmentWindowClose)

function equipClick(button, state)
	if state == "down" then
		if source == cuffButton then
			triggerServerEvent("onClientPoliceEquipmentClick", localPlayer, "cuff")
		elseif source == batonButton then
			triggerServerEvent("onClientPoliceEquipmentClick", localPlayer, "baton")
		elseif source == tazerButton then
			triggerServerEvent("onClientPoliceEquipmentClick", localPlayer, "tazer")
		elseif source == pistolButton then
			triggerServerEvent("onClientPoliceEquipmentClick", localPlayer, "pistol")
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", root, equipClick)