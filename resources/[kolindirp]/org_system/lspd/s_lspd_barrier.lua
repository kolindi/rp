function barrierinit()
	barrier = createObject(968, 1544.65, -1623.965, 13.05, 0, -90, 90)
	--enterMarker = createMarker(1539.927734375, -1627.595703125, 13.38281255)
	--exitMarker = createMarker(1550.85546875, -1627.705078125, 13.382812555)
	triggerMarker = createMarker(1544.728515625, -1627.0263671875, 12.4, "cylinder", 10, 0, 0, 255, 0, nil)
	open = false
	isMove = false
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), barrierinit)


function barrierOpen(hitElement, matchingDimension)
	if source == triggerMarker then
		if isMove == false and open == false then
			if getElementType(hitElement) == "vehicle" then
				if getElementModel(hitElement) == 596 or getElementModel(hitElement) == 427 or getElementModel(hitElement) == 601 or getElementModel(hitElement) == 523 then
					moveObject(barrier, 3000, 1544.65, -1623.965, 13.05, 0, 90)
					isMove = true
					open = true
					setTimer(function()
						isMove = false
						--open = true
						barrierClose()
					end, 8000, 1)
				end
			end
		end
	end
end
addEventHandler( "onMarkerHit", getRootElement(), barrierOpen)

--[[function barrierClose(leftElement)
	if source == triggerMarker then
		if isMove == false then
			moveObject(barrier, 3000, 1544.65, -1623.965, 13.05, 0, -90)
			isMove = true
			setTimer(function()
				isMove = false
				open = true
			end, 3000, 1)
		elseif isMove == true then
			setTimer(function()
				moveObject(barrier, 3000, 1544.65, -1623.965, 13.05, 0, -90)
				isMove = false
				open = false
			end, 3000, 1)
		end
	end
end
addEventHandler("onMarkerLeave", getRootElement(), barrierClose)
]]


function barrierClose()
	moveObject(barrier, 3000, 1544.65, -1623.965, 13.05, 0, -90)
	setTimer(function()
		isMove = false
		open = false
	end, 3000, 1)
end
