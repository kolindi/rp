database = call(getResourceFromName("db"), "db")
function createGang(gangName, lastRankName)
	local query = dbQuery(database, "SELECT name FROM organizations WHERE name = '"..gangName.."';")
	local result, num_rows = dbPoll(query, -1)
	if num_rows > 0 then
		exports.ns:sendNotification(source, "abort", "Это имя банды уже занято!")
	else
		dbExec(database, "INSERT INTO organizations (name, rank10) VALUES ('"..tostring(gangName).."', '"..tostring(lastRankName).."');")
		dbExec(database, "UPDATE accounts SET org_name = '"..tostring(gangName).."' WHERE name = '"..getPlayerName(source).."';")
		dbExec(database, "UPDATE accounts SET rank = '"..tonumber(10).."' WHERE name = '"..getPlayerName(source).."';")
		exports.ns:sendNotification(source, "success", "Банда "..gangName.." успешна создана!")
		exports.ns:sendNotification(source, "info", "Вы назначены "..lastRankName.." в "..gangName.."!")
		setElementData(source, "org", gangName)
		setElementData(source, "rank", 10)
		local gangTeam = createTeam(gangName)
		setPlayerTeam(source, gangTeam)
	end
end
addEventHandler("onClientCreateGangButtonClick", getRootElement(), createGang)