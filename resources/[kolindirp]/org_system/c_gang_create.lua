CGGUIOpen = false
CGGUI = {}
function createGangGUI(commandName)
	if CGGUIOpen == false then
		CGGUIOpen = true
		DGS = exports.dgs
		CGGUI.window = DGS:dgsDxCreateWindow(0.25, 0.25, 0.50, 0.40, "Создание банды", true)
		CGGUI.gangImg = DGS:dgsDxCreateImage(0.015, 0, 0.10, 0.15, ":org_system/img/gang.png", true, CGGUI.window)
		CGGUI.info = DGS:dgsDxCreateLabel(0.12, 0, 0.10, 0.15, "В этом окне вы можете создать банду. Но учтите, что первое время\nбанда будет иметь ограничения:\n1. Дом банды можно создать при численности банды от 5 человек.\n2. Захват территорий доступен при численности от 10 человек.", true, CGGUI.window)
		CGGUI.name = DGS:dgsDxCreateEdit(0.40, 0.30, 0.50, 0.05, "", true, CGGUI.window)
		CGGUI.nameLabel = DGS:dgsDxCreateLabel(0.025, 0.30, 0.20, 0.10, "Название банды (латиница):", true, CGGUI.window)
		CGGUI.lastRankName = DGS:dgsDxCreateEdit(0.40, 0.40, 0.50, 0.05, "", true, CGGUI.window)
		CGGUI.lastRankNameLabel = DGS:dgsDxCreateLabel(0.025, 0.40, 0.20, 0.10, "Название высшего(10) ранга:", true, CGGUI.window)
		CGGUI.createButton = DGS:dgsDxCreateButton(0.35, 0.60, 0.30, 0.10, "Создать банду", true, CGGUI.window)
	end
end
addCommandHandler("createGang", createGangGUI)

function CGGUIClosed()
	if source == CGGUI.window then
		CGGUIOpen = false
	end
end
addEventHandler("onClientDgsDxWindowClose", getResourceRootElement(getThisResource()), CGGUIClosed)

function createButtonClicked(button, state)
	if source == CGGUI.createButton then
		if state == "up" then
			--if getElementData(localPlayer, "org") == "Citizen" then
				local gangName, lastRankName = DGS:dgsDxGUIGetText(CGGUI.name), DGS:dgsDxGUIGetText(CGGUI.lastRankName)
				triggerServerEvent("onClientCreateGangButtonClick", localPlayer, gangName, lastRankName)
				destroyElement(CGGUI.window)
			--else
				--exports.ns:sendNotification(localPlayer, "abort", "Вы уже состоите в организации")
				--destroyElement(CGGUI.window)
			--end
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), createButtonClicked)