DGS = exports.dgs
omGUI = {}
omGUIOpen = false
function orgMenu()
	pOrg = getPlayerTeam(localPlayer)
	if getElementData(pOrg, "type") == 10 then
		exports.ns:sendCNotification("error", "Вы гражданский...")
	else
		if omGUIOpen == false then
			omGUI.window = DGS:dgsDxCreateWindow(0.7, 0.30, 0.26, 0.65, "Меню организации", true)
			DGS:dgsDxWindowSetSizable(omGUI.window, false)
			omGUI.playersCount = DGS:dgsDxCreateButton(0, 0.35, 1, 0.1, "", true, omGUI.window)
			if getElementData(localPlayer, "rank") == 10 then
				omGUI.budgetButton = DGS:dgsDxCreateButton(0, 0.57, 1, 0.1, "Настройка зарплат", true, omGUI.window)
			end
			orgName = getTeamName(pOrg)
			if getElementData(pOrg, "type") ~= 2 then
				omGUI.payButton = DGS:dgsDxCreateButton(0, 0.46, 1, 0.1, "Настройка выплат", true, omGUI.window)
			end
			if getElementData(pOrg, "type") == 1 then
				if getElementData(pOrg, "name") == "Police" then
					omGUI.logo = DGS:dgsDxCreateImage(0.44, 0.07, 0.15, 0.20, ":org_system/img/police.png", true, omGUI.window)
					DGS:dgsDxGUISetText(omGUI.playersCount, "Сотрудники на службе")
					omGUI.name = DGS:dgsDxCreateLabel(0.45, 0, 1, 0.1, getElementData(pOrg, "rusname"), true, omGUI.window)
				end
			elseif getElementData(pOrg, "type") == 2 then
				omGUI.logo = DGS:dgsDxCreateImage(0.44, 0.07, 0.15, 0.20, ":org_system/img/gang.png", true, omGUI.window)
				omGUI.name = DGS:dgsDxCreateLabel(0.465, 0, 1, 0.1, getElementData(pOrg, "name"), true, omGUI.window)
				DGS:dgsDxGUISetText(omGUI.playersCount, "Бандиты на районе")
				if getElementData(localPlayer, "rank") == 10 then
					omGUI.rankNameButton = DGS:dgsDxCreateButton(0, 0.68, 1, 0.1, "Настройка должностей", true, omGUI.window)
				end
			elseif getElementData(pOrg, "type") == 4 then
				omGUI.logo = DGS:dgsDxCreateImage(0.385, 0.02, 0.20, 0.25, ":org_system/img/gov.png", true, omGUI.window)
				DGS:dgsDxGUISetText(omGUI.playersCount, "Сотрудники на работе")
				omGUI.name = DGS:dgsDxCreateLabel(0.375, 0, 1, 0.1, getElementData(pOrg, "rusname"), true, omGUI.window)
				if getElementData(localPlayer, "rank") == 10 then
					omGUI.taxButton = DGS:dgsDxCreateButton(0, 0.68, 1, 0.1, "Настройка налогов", true, omGUI.window)
				end
			end
			omGUI.budgetLabel = DGS:dgsDxCreateLabel(0.40, 0.25, 0.5, 0.2, "Бюджет: "..getElementData(pOrg, "budget").."", true, omGUI.window)
		end
	end
end
addCommandHandler("om", orgMenu)

function buttonClicked(button, state, x, y)
	if state == "up" then
		if source == omGUI.backButton then
			destroyElement(omGUI.window)
			omGUIOpen = false
			orgMenu()
			if omGUI.uvalButton ~= nil then
				destroyElement(omGUI.uvalButton)
				destroyElement(omGUI.upRankButton)
				destroyElement(omGUI.downRankButton)
				destroyElement(omGUI.premButton)
			end
		elseif source == omGUI.playersCount then
			destroyOmGUIGeneral()
			omGUI.backButton = DGS:dgsDxCreateButton(0, 0.90, 1, 0.1, "Назад", true, omGUI.window)
			omGUI.countLabel = DGS:dgsDxCreateLabel(0.01, 0.01, 1, 0.1, "Всего людей: "..countPlayersInTeam(pOrg).."", true, omGUI.window)
			omGUI.countGrid = DGS:dgsDxCreateGridList(0, 0.1, 1, 0.8, true, omGUI.window)
			omGUI.playersColumn = DGS:dgsDxGridListAddColumn(omGUI.countGrid, "Имя и Фамилия", 0.35)
			omGUI.rankColumn = DGS:dgsDxGridListAddColumn(omGUI.countGrid, "Должность", 0.10)
			local playersInTeam = getPlayersInTeam(pOrg)
			for _, thePlayer in ipairs(playersInTeam) do
				local row = DGS:dgsDxGridListAddRow(omGUI.countGrid)
				DGS:dgsDxGridListSetItemText(omGUI.countGrid, row, omGUI.playersColumn, getPlayerName(thePlayer), false, false)
				DGS:dgsDxGridListSetItemText(omGUI.countGrid, row, omGUI.rankColumn, ""..getElementData(pOrg, "r"..getElementData(thePlayer, "rank").."").."("..getElementData(thePlayer, "rank")..")", false, false)
			end
		elseif source == omGUI.payButton then
			destroyOmGUIGeneral()
			omGUI.payBankButton = DGS:dgsDxCreateButton(0, 0.90, 1, 0.1, "Сохранить", true, omGUI.window)
			omGUI.payInfoText = DGS:dgsDxCreateLabel(0.01, 0.01, 0.1, 1, "Введите номер банковского счёта,\nна который будут происходить выплаты ваших зарплат и\nпремий.\nУчтите, что если номер счёта будет введён неверно,\nто вы не будете получать зарплату.", true, omGUI.window)
			omGUI.payBank = DGS:dgsDxCreateEdit(0.01, 0.2, 0.98, 0.1, getElementData(localPlayer, "paydayBankAccount"), true, omGUI.window, FF000000, 1, 2.5)
		elseif source == omGUI.payBankButton then
			bankAccount = tonumber(DGS:dgsDxGUIGetText(omGUI.payBank))
			triggerServerEvent("onClientChangePaydayBankAccount", localPlayer, bankAccount)
			destroyElement(omGUI.window)
			omGUIOpen = false
			orgMenu()
		elseif source == omGUI.budgetButton then
			destroyOmGUIGeneral()
			omGUI.paySave = DGS:dgsDxCreateButton(0, 0.95, 1, 0.05, "Сохранить", true, omGUI.window)
			omGUI.payR1Text = DGS:dgsDxCreateLabel(0, 0.01, 0.1, 0.1, ""..getElementData(pOrg, "r1").."", true, omGUI.window)
			omGUI.payR2Text = DGS:dgsDxCreateLabel(0, 0.10, 0.1, 0.1, ""..getElementData(pOrg, "r2").."", true, omGUI.window)
			omGUI.payR3Text = DGS:dgsDxCreateLabel(0, 0.20, 0.1, 0.1, ""..getElementData(pOrg, "r3").."", true, omGUI.window)
			omGUI.payR4Text = DGS:dgsDxCreateLabel(0, 0.30, 0.1, 0.1, ""..getElementData(pOrg, "r4").."", true, omGUI.window)
			omGUI.payR5Text = DGS:dgsDxCreateLabel(0, 0.40, 0.1, 0.1, ""..getElementData(pOrg, "r5").."", true, omGUI.window)
			omGUI.payR6Text = DGS:dgsDxCreateLabel(0, 0.50, 0.1, 0.1, ""..getElementData(pOrg, "r6").."", true, omGUI.window)
			omGUI.payR7Text = DGS:dgsDxCreateLabel(0, 0.60, 0.1, 0.1, ""..getElementData(pOrg, "r7").."", true, omGUI.window)
			omGUI.payR8Text = DGS:dgsDxCreateLabel(0, 0.70, 0.1, 0.1, ""..getElementData(pOrg, "r8").."", true, omGUI.window)
			omGUI.payR9Text = DGS:dgsDxCreateLabel(0, 0.80, 0.1, 0.1, ""..getElementData(pOrg, "r9").."", true, omGUI.window)
			omGUI.payR10Text = DGS:dgsDxCreateLabel(0, 0.90, 0.1, 0.1, ""..getElementData(pOrg, "r10").."", true, omGUI.window)
			omGUI.payR1 = DGS:dgsDxCreateEdit(0.45, 0.01, 0.5, 0.04, ""..getElementData(pOrg, "pay1").."", true, omGUI.window)
			omGUI.payR2 = DGS:dgsDxCreateEdit(0.45, 0.10, 0.5, 0.04, ""..getElementData(pOrg, "pay2").."", true, omGUI.window)
			omGUI.payR3 = DGS:dgsDxCreateEdit(0.45, 0.20, 0.5, 0.04, ""..getElementData(pOrg, "pay3").."", true, omGUI.window)
			omGUI.payR4 = DGS:dgsDxCreateEdit(0.45, 0.30, 0.5, 0.04, ""..getElementData(pOrg, "pay4").."", true, omGUI.window)
			omGUI.payR5 = DGS:dgsDxCreateEdit(0.45, 0.40, 0.5, 0.04, ""..getElementData(pOrg, "pay5").."", true, omGUI.window)
			omGUI.payR6 = DGS:dgsDxCreateEdit(0.45, 0.50, 0.5, 0.04, ""..getElementData(pOrg, "pay6").."", true, omGUI.window)
			omGUI.payR7 = DGS:dgsDxCreateEdit(0.45, 0.60, 0.5, 0.04, ""..getElementData(pOrg, "pay7").."", true, omGUI.window)
			omGUI.payR8 = DGS:dgsDxCreateEdit(0.45, 0.70, 0.5, 0.04, ""..getElementData(pOrg, "pay8").."", true, omGUI.window)
			omGUI.payR9 = DGS:dgsDxCreateEdit(0.45, 0.80, 0.5, 0.04, ""..getElementData(pOrg, "pay9").."", true, omGUI.window)
			omGUI.payR10 = DGS:dgsDxCreateEdit(0.45, 0.90, 0.5, 0.04, ""..getElementData(pOrg, "pay10").."", true, omGUI.window)
		elseif source == omGUI.paySave then
			p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 = DGS:dgsDxGUIGetText(omGUI.payR1), DGS:dgsDxGUIGetText(omGUI.payR2), DGS:dgsDxGUIGetText(omGUI.payR3), DGS:dgsDxGUIGetText(omGUI.payR4), DGS:dgsDxGUIGetText(omGUI.payR5), DGS:dgsDxGUIGetText(omGUI.payR6), DGS:dgsDxGUIGetText(omGUI.payR7), DGS:dgsDxGUIGetText(omGUI.payR8), DGS:dgsDxGUIGetText(omGUI.payR9), DGS:dgsDxGUIGetText(omGUI.payR10)
			triggerServerEvent("onLeaderChangePayday", pOrg, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, localPlayer)
			destroyElement(omGUI.window)
			omGUIOpen = false
			orgMenu()
		elseif source == omGUI.rankNameButton then
			destroyOmGUIGeneral()
			omGUI.backButton = DGS:dgsDxCreateButton(0, 0.90, 1, 0.1, "Назад", true, omGUI.window)
		elseif source == omGUI.countGrid then
			selected = DGS:dgsDxGridListGetSelectedItem(omGUI.countGrid)
			if selected ~= -1 then
				if omGUI.uvalButton ~= nil then
					destroyElement(omGUI.uvalButton)
					destroyElement(omGUI.upRankButton)
					destroyElement(omGUI.downRankButton)
					destroyElement(omGUI.premButton)
				end
				name = DGS:dgsDxGridListGetItemText(omGUI.countGrid, selected, omGUI.playersColumn)
				rank = getElementData(getPlayerFromName(name), "rank")
				if getElementData(localPlayer, "rank") > 8 and getElementData(localPlayer, "rank") > rank then
					omGUI.uvalButton = DGS:dgsDxCreateButton(x, y, 80, 30, "Уволить", false)
					DGS:dgsDxGUIBringToFront(omGUI.uvalButton)
					if getElementData(getPlayerFromName(name), "rank") ~= 1 or getElementData(getPlayerFromName(name), "rank") ~= 9 or getElementData(getPlayerFromName(name), "rank") ~= 10 or getElementData(getPlayerFromName(name)) ~= localPlayer then
						omGUI.upRankButton = DGS:dgsDxCreateButton(x, y+30, 80, 30, "Повысить", false)
						omGUI.downRankButton = DGS:dgsDxCreateButton(x, y+60, 80, 30, "Понизить", false)
						omGUI.premButton = DGS:dgsDxCreateButton(x, y+90, 80, 30, "Премия", false)
						DGS:dgsDxGUIBringToFront(omGUI.upRankButton)
						DGS:dgsDxGUIBringToFront(omGUI.downRankButton)
						DGS:dgsDxGUIBringToFront(omGUI.premButton)
					end
				end
			else
				if omGUI.uvalButton ~= nil then
					destroyGridButtons()
				end
			end
		elseif source == omGUI.premButton then
			omGUI.premWindow = DGS:dgsDxCreateWindow(0.50, 0.30, 0.35, 0.3, "Выписать премию", true)
			omGUI.premText = DGS:dgsDxCreateLabel(0.05, 0.1, 1, 0.1, "Введите сумму премии.\nОна будет направлена из вашего бюджета на банковский аккаунт работника.", true, omGUI.premWindow)
			omGUI.premEdit = DGS:dgsDxCreateEdit(0.1, 0.5, 0.8, 0.2, "", true, omGUI.premWindow)
			omGUI.premWindowButton = DGS:dgsDxCreateButton(0.3, 0.75, 0.4, 0.22, "Выписать премию", true, omGUI.premWindow)
		elseif source == omGUI.premWindowButton then
			prem = tonumber(DGS:dgsDxGUIGetText(omGUI.premEdit))
			if getElementData(pOrg, "budget") > prem then
				triggerServerEvent("onOfficerPremPlayer", localPlayer, getPlayerFromName(name), prem)
				exports.ns:sendCNotification("success", "Премия работнику "..name.." суммой "..prem.." Колиндов успешно выплачена!")
				destroyElement(omGUI.premWindow)
			else
				exports.ns:sendCNotification("abort", "В бюджете вашей организации недостаточно денег!")
				destroyElement(omGUI.premWindow)
			end
		elseif source == omGUI.uvalButton then
			omGUI.uvalWindow = DGS:dgsDxCreateWindow(0.50, 0.30, 0.35, 0.3, "Уволить члена организации", true)
			omGUI.uvalText = DGS:dgsDxCreateLabel(0.1, 0.1, 1, 0.1, "Введите причину увольнения. \nУчтите, что все данные будут сохранены.", true, omGUI.uvalWindow)
			omGUI.uvalReason = DGS:dgsDxCreateEdit(0.1, 0.5, 0.8, 0.2, "", true, omGUI.uvalWindow)
			omGUI.uvalWindowButton = DGS:dgsDxCreateButton(0.3, 0.75, 0.4, 0.22, "Уволить", true, omGUI.uvalWindow)
		elseif source == omGUI.uvalWindowButton then
			reason = DGS:dgsDxGUIGetText(omGUI.uvalReason)
			if utf8.len(reason) < 3 then
				exports.ns:sendCNotification("error", "Причина слишком короткая!")
			else
				triggerServerEvent("onOfficerUvalPlayer", getPlayerFromName(name), localPlayer, reason)
				destroyElement(omGUI.uvalWindow)
				destroyElement(omGUI.uvalButton)
				DGS:dgsDxGridListRemoveRow(omGUI.countGrid, selected)
				if getPlayerFromName(name) == localPlayer then
					destroyGridButtons()
					destroyElement(omGUI.window)
				end
			end
		elseif source == omGUI.upRankButton then
			destroyGridButtons()
			triggerServerEvent("onOfficerUpRankPlayer", getPlayerFromName(name), localPlayer)
		elseif source == omGUI.downRankButton then
			destroyGridButtons()
			triggerServerEvent("onOfficerDownRankPlayer", getPlayerFromName(name), localPlayer)
		elseif source == omGUI.taxButton then

		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), buttonClicked)

function destroyGridButtons()
	destroyElement(omGUI.uvalButton)
	destroyElement(omGUI.upRankButton)
	destroyElement(omGUI.downRankButton)
	destroyElement(omGUI.premButton)
end

function omGUIClosed()
	if source == omGUI.window then
		omGUIOpen = false
		if omGUI.uvalButton ~= nil then
			destroyGridButtons()
		end
	end
end
addEventHandler("onClientDgsDxWindowClose", getRootElement(), omGUIClosed)

function destroyOmGUIGeneral()
	destroyElement(omGUI.playersCount)
	destroyElement(omGUI.budgetButton)
	destroyElement(omGUI.logo)
	destroyElement(omGUI.name)
	destroyElement(omGUI.budgetLabel)
	destroyElement(omGUI.rankNameButton)
	destroyElement(omGUI.payButton)
end