database = call(getResourceFromName("db"), "getDb")
function orgInit()
	local query = dbQuery(database, "SELECT * FROM organizations;")
	local result = dbPoll(query, -1, false)
	for id, row in ipairs(result) do
		org = createTeam(row['name'])
		if row['rus_name'] ~= "" or row['rus_name'] ~= false then
			setElementData(org, "rusname", row['rus_name'])
		end
		setElementData(org, "name", row['name'])
		setElementData(org, "spawn_int", row['spawn_int'])
		setElementData(org, "spawn_x", row['spawn_x'])
		setElementData(org, "spawn_y", row['spawn_y'])
		setElementData(org, "spawn_z", row['spawn_z'])
		setElementData(org, "type", tonumber(row['type']))
		setElementData(org, "budget", tonumber(row['budget']))

		setElementData(org, "red", tonumber(row['red']))
		setElementData(org, "green", tonumber(row['green']))
		setElementData(org, "blue", tonumber(row['blue']))

		setElementData(org, "attack", false)

		setElementData(org, "r1", row['rank1'])
		setElementData(org, "r2", row['rank2'])
		setElementData(org, "r3", row['rank3'])
		setElementData(org, "r4", row['rank4'])
		setElementData(org, "r5", row['rank5'])
		setElementData(org, "r6", row['rank6'])
		setElementData(org, "r7", row['rank7'])
		setElementData(org, "r8", row['rank8'])
		setElementData(org, "r9", row['rank9'])
		setElementData(org, "r10", row['rank10'])

		setElementData(org, "pay1", tonumber(row['payday1']))
		setElementData(org, "pay2", tonumber(row['payday2']))
		setElementData(org, "pay3", tonumber(row['payday3']))
		setElementData(org, "pay4", tonumber(row['payday4']))
		setElementData(org, "pay5", tonumber(row['payday5']))
		setElementData(org, "pay6", tonumber(row['payday6']))
		setElementData(org, "pay7", tonumber(row['payday7']))
		setElementData(org, "pay8", tonumber(row['payday8']))
		setElementData(org, "pay9", tonumber(row['payday9']))
		setElementData(org, "pay10", tonumber(row['payday10']))
		--outputDebugString("Организация "..getTeamName(org).." загружена!")
	end
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), orgInit)

function createOrg(playerSource, commandName, name, rusname)
	if hasObjectPermissionTo(playerSource, "command.createOrg") then
		local query = dbExec(database, "INSERT INTO organizations (name, rus_name) VALUES ('"..tostring(name).."', '"..tostring(rusname).."');")
		if query then
			exports.ns:sendNotification(playerSource, "success", "Организация "..tostring(name).." ("..tostring(rusname)..") создана!")
		else
			--outputChatBox("Ошибка создания организации. Проверьте правильность ввода. /createOrg name rusname", playerSource)
			exports.ns:sendNotification(playerSource, "error", "Ошибка создания организации. Проверьте правильность ввода. /createOrg name rusname")
		end
	end
end
addCommandHandler("createOrg", createOrg)

function carsInit()
	local query = dbQuery(database, "SELECT * FROM gov_cars;")
	local result = dbPoll(query, -1)
	for _, row in ipairs(result) do
		govCar = createVehicle(row['car_id'], row['x'], row['y'], row['z'], row['rx'], row['ry'], row['rz'])
		setElementData(govCar, "org", row['org'])
		setElementData(govCar, "fuel", 100)
		setElementData(govCar, "engineState", 0)
		setElementData(govCar, "lightState", 0)
		setVehicleOverrideLights(source, 1)
		--outputDebugString("Машина №"..row['id'].." организации "..row['org'].." создана!")
	end
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), carsInit)

function addCar(adminSource, commandName, orgName)
	if hasObjectPermissionTo(adminSource, "command.addCar") then
		if orgName == "" or getPedOccupiedVehicle(adminSource) == false then
			exports.ns:sendNotification(adminSource, "error", "ОШИБКА! Не введено название организации или вы не в автомобиле")
		else
			local carId = getElementModel(getPedOccupiedVehicle(adminSource))
			--outputDebugString(carId)
			local x, y, z = getElementPosition(getPedOccupiedVehicle(adminSource))
			--outputDebugString(x.." "..y.." "..z)
			local rx, ry, rz = getElementRotation(getPedOccupiedVehicle(adminSource), "ZYX")
			dbExec(database, "INSERT INTO gov_cars (car_id, org, x, y, z, rx, ry, rz) VALUES ('"..tonumber(carId).."', '"..tostring(orgName).."', '"..tonumber(x).."', '"..tonumber(y).."', '"..tonumber(z).."', '"..tonumber(rx).."', '"..tonumber(ry).."', '"..tonumber(rz).."');")
			exports.ns:sendNotification(adminSource, "success", "Автомобиль для организации "..orgName.." добавлен!")
		end
	end
end
addCommandHandler("addCar", addCar)

function uvalPlayer(by, reason)
	setElementData(source, "org", "Citizen")
	setPlayerTeam(source, getTeamFromName("Citizen"))
	exports.ns:sendNotification(source, "info", ""..getPlayerName(by).." уволил вас по причине: "..reason.."!")
	dbExec(database, "UPDATE accounts SET org_name = 'Citizen' WHERE name = '"..getPlayerName(source).."';")
	dbExec(database, "UPDATE accounts SET rank = '"..tonumber(0).."' WHERE name = '"..getPlayerName(source).."';")
	dbExec(database, "INSERT INTO firedPlayers (pId, byId, reason) VALUES ('"..tonumber(getElementData(source, "id")).."', '"..tonumber(getElementData(by, "id")).."', '"..reason.."');")
end
addEventHandler("onOfficerUvalPlayer", getRootElement(), uvalPlayer)

function upRank(by)
	local newRank = getElementData(source, "rank") + 1
	setElementData(source, "rank", newRank)
	dbExec(database, "UPDATE accounts SET rank = '"..tonumber(newRank).."' WHERE name = '"..getPlayerName(source).."';")
	exports.ns:sendNotification(source, "info", ""..getPlayerName(by).." повысил вас!")
end
addEventHandler("onOfficerUpRankPlayer", getRootElement(), upRank)

function downRank(by)
	local newRank = getElementData(source, "rank") - 1
	setElementData(source, "rank", newRank)
	dbExec(database, "UPDATE accounts SET rank = '"..tonumber(newRank).."' WHERE name = '"..getPlayerName(source).."';")
	exports.ns:sendNotification(source, "info", ""..getPlayerName(by).." понизил вас!")
end
addEventHandler("onOfficerDownRankPlayer", getRootElement(), downRank)

function getPlayerRankName(playerSource)
	local rank = getElementData(playerSource, "rank")
	local org = getPlayerTeam(playerSource)
	if org == "Citizen" then
		return
	else
		rankName = getElementData(org, "r"..rank.."")
		return rankName
	end
end

function changePayDay(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, by)
	orgName = getTeamName(source)
	setElementData(source, "pay1", p1)
	setElementData(source, "pay2", p2)
	setElementData(source, "pay3", p3)
	setElementData(source, "pay4", p4)
	setElementData(source, "pay5", p5)
	setElementData(source, "pay6", p6)
	setElementData(source, "pay7", p7)
	setElementData(source, "pay8", p8)
	setElementData(source, "pay9", p9)
	setElementData(source, "pay10", p10)
	dbExec(database, "UPDATE organizations SET payday1 = '"..tonumber(p1).."' WHERE name = '"..orgName.."';")
	dbExec(database, "UPDATE organizations SET payday2 = '"..tonumber(p2).."' WHERE name = '"..orgName.."';")
	dbExec(database, "UPDATE organizations SET payday3 = '"..tonumber(p3).."' WHERE name = '"..orgName.."';")
	dbExec(database, "UPDATE organizations SET payday4 = '"..tonumber(p4).."' WHERE name = '"..orgName.."';")
	dbExec(database, "UPDATE organizations SET payday5 = '"..tonumber(p5).."' WHERE name = '"..orgName.."';")
	dbExec(database, "UPDATE organizations SET payday6 = '"..tonumber(p6).."' WHERE name = '"..orgName.."';")
	dbExec(database, "UPDATE organizations SET payday7 = '"..tonumber(p7).."' WHERE name = '"..orgName.."';")
	dbExec(database, "UPDATE organizations SET payday8 = '"..tonumber(p8).."' WHERE name = '"..orgName.."';")
	dbExec(database, "UPDATE organizations SET payday9 = '"..tonumber(p9).."' WHERE name = '"..orgName.."';")
	dbExec(database, "UPDATE organizations SET payday10 = '"..tonumber(p10).."' WHERE name = '"..orgName.."';")
	exports.ns:sendNotification(by, "success", "Настройки зарплат успешно изменены!")
end
addEventHandler("onLeaderChangePayday", getRootElement(), changePayDay)

function changePaydayBankAccount(bankAccount)
	local query = dbExec(database, "UPDATE accounts SET paydayBankAccount = '"..bankAccount.."' WHERE id = '"..getElementData(source, "id").."';")
	setElementData(source, "paydayBankAccount", bankAccount)
	exports.ns:sendNotification(source, "success", "Номер банковского счёта для выплат успешно сохранён!")
end
addEventHandler("onClientChangePaydayBankAccount", getRootElement(), changePaydayBankAccount)

function payday()
	local players = getElementsByType("player")
	for _, playerSource in ipairs(players) do
		local org = getPlayerTeam(playerSource)
		if getTeamName(org) ~= "Citizen" and getElementData(org, "type") == 1 then
			local orgBalance = getElementData(org, "budget")
			local playerRank = getElementData(playerSource, "rank")
			local rankPayday = getElementData(org, "pay"..playerRank.."")
			if orgBalance > rankPayday then
				local bankAccount = getElementData(playerSource, "paydayBankAccount")
				local orgBalance = orgBalance - rankPayday
				local query = dbExec(database, "UPDATE organizations SET budget = '"..orgBalance.."' WHERE name = '"..getTeamName(org).."';")
				local query = dbExec(database, "UPDATE bankAccounts SET balance = balance + '"..rankPayday.."' WHERE id = '"..bankAccount.."';")
				setElementData(org, "budget", orgBalance)
			end
		end
	end
end
addEventHandler("payday", getRootElement(), payday)

function premium(playerSource, prem)
	local bankAccount = getElementData(playerSource, "paydayBankAccount")
	local org = getPlayerTeam(playerSource)
	local orgName = getTeamName(org)
	local query = dbExec(database, "UPDATE bankAccounts SET balance = balance + '"..prem.."' WHERE id = '"..bankAccount.."';")
	local query = dbExec(database, "UPDATE organizations SET budget = budget - '"..prem.."' WHERE name = '"..orgName.."';")
	setElementData(org, "budget", getElementData(org, "budget") - prem)
end
addEventHandler("onOfficerPremPlayer", getRootElement(), premium)