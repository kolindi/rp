function governmentInit()
	governmentEnter = createPickup(1481.15, -1771.9287109375, 18.795755386353, 3, 1318, 1)
	setElementData(governmentEnter, "text", "Здание государства")
	governmentExit = createPickup(390.76953125, 173.8037109375, 1008.3828125, 3, 1318, 1)
	setElementData(governmentExit, "text", "Выход")
	setElementInterior(governmentExit, 3)
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), governmentInit)

function governmentPickupHit(playerSource)
	if getElementType(playerSource) == "player" then
		if source == governmentEnter then
			setElementInterior(playerSource, 3)
			setElementPosition(playerSource, 384.808624,173.804992,1008.382812)
		elseif source == governmentExit then
			setElementInterior(playerSource, 0)
			setElementPosition(playerSource, 1481.15, -1766.9287109375, 18.795755386353)
		end
	end
end
addEventHandler("onPickupHit", getRootElement(), governmentPickupHit)