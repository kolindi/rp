function getHouseByID(id)
	local houses = getElementsByType("pickup")
	for key, house in ipairs(houses) do
		if getElementData(house, "id") == id then
			return house
		end
	end
end