function EnterHouse(houseSource)
	local x, y, z = getElementPosition(source)
	setElementData(source, "exit_x", x)
	setElementData(source, "exit_y", y)
	setElementData(source, "exit_z", z)
	setElementInterior(source, getElementData(houseSource, "int_id"))
	setElementPosition(source, getElementData(houseSource, "int_x"), getElementData(houseSource, "int_y"), getElementData(houseSource, "int_z"))
	setElementDimension(source, getElementData(houseSource, "id") + 100)
	setElementData(source, "inHouse", 1)
end
addEventHandler("onClientEnterHouse", getRootElement(), EnterHouse)

function ExitHouse()
	setElementInterior(source, 0)
	setElementDimension(source, 0)
	setElementPosition(source, getElementData(source, "exit_x"), getElementData(source, "exit_y"), getElementData(source, "exit_z"))
	setElementData(source, "inHouse", 0)
end
addEventHandler("onClientExitHouseClick", getRootElement(), ExitHouse)