function garageInit()
	local query = dbQuery(database, "SELECT * FROM garage;")
	local result = dbPoll(query, -1, false)
	for rid, row in ipairs(result) do
		if row['owner_id'] and row['vehicle'] ~= nil then
			playerVehicle = createVehicle(row['vehicle'], row['x'], row['y'], row['z'], row['rx'], row['ry'], row['rz'])
			setVehicleEngineState(playerVehicle, false)
			setElementData(playerVehicle, "owner", row['owner_id'])
			setElementData(playerVehicle, "engineState", 0)
			setElementData(playerVehicle, "fuel", 100)
			setElementData(playerVehicle, "lightState", 0)
			setVehicleLocked(playerVehicle, true)
			setVehicleOverrideLights(playerVehicle, 1)
			--setElementID(playerVehicle, row['id'])
			--local owner = exports.id_system:getPlayerByID(row['owner_id'])
			setElementData(playerVehicle, "id", row['id'])
		end
	end
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), garageInit)