function houseinit()
	house = {}
	local query = dbQuery(database, "SELECT * FROM house;")
	local result = dbPoll(query, -1, false)
	for rid, row in pairs(result) do
		if row['Owner'] ~= 0 then
			houseBlip = createBlip(row['x'], row['y'], row['z'], 32, 2, 255, 0, 0, 255, 0, 150)
			housePickup = createPickup(row['x'], row['y'], row['z'], 3, 1272, 1)
		else
			houseBlip = createBlip(row['x'], row['y'], row['z'], 31, 2, 255, 0, 0, 255, 0, 150)
			housePickup = createPickup(row['x'], row['y'], row['z'], 3, 1273, 1)
		end
		setElementData(housePickup, "int_id", row['int_id'], true)
		setElementData(housePickup, "int_x", row['int_x'], true)
		setElementData(housePickup, "int_y", row['int_y'], true)
		setElementData(housePickup, "int_z", row['int_z'], true)
		setElementData(housePickup, "price", row['price'], true)
		setElementData(housePickup, "id", row['id'], true)
		setElementData(housePickup, "isOpen", row['isOpen'], true)
		setElementData(housePickup, "picType", "house", true)
		if row['Owner'] == 0 then
			setElementData(housePickup, "isOpen", 1, true)
		end
		outputDebugString("Дом № "..getElementData(housePickup, "id").." загружен! Цена: "..getElementData(housePickup, "price").."")
	end
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), houseinit)