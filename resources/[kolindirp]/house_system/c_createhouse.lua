function createHouseGui(commandName, price)
	createHouseGUI = {
	    label = {}
	}
	outputDebugString(price)
	showCursor(true)
	createHouseGUI.window = guiCreateWindow(0.33, 0.40, 0.35, 0.19, "Интерьер дома", true)
	guiWindowSetSizable(createHouseGUI.window, false)
	
	createHouseGUI.x = guiCreateEdit(0.26, 0.29, 0.69, 0.10, "", true, createHouseGUI.window)
	createHouseGUI.y = guiCreateEdit(0.26, 0.45, 0.69, 0.10, "", true, createHouseGUI.window)
	createHouseGUI.z = guiCreateEdit(0.26, 0.60, 0.69, 0.10, "", true, createHouseGUI.window)
	createHouseGUI.indid = guiCreateEdit(0.26, 0.16, 0.22, 0.10, "", true, createHouseGUI.window)
	createHouseGUI.label[1] = guiCreateLabel(0.15, 0.17, 0.09, 0.09, "Int Id:", true, createHouseGUI.window)
	createHouseGUI.label[2] = guiCreateLabel(0.15, 0.30, 0.09, 0.09, "x:", true, createHouseGUI.window)
	createHouseGUI.label[3] = guiCreateLabel(0.15, 0.45, 0.09, 0.09, "y:", true, createHouseGUI.window)
	createHouseGUI.label[4] = guiCreateLabel(0.15, 0.59, 0.09, 0.09, "z:", true, createHouseGUI.window)
	createHouseGUIcreate = guiCreateButton(0.29, 0.77, 0.41, 0.18, "Создать дом", true, createHouseGUI.window)
	function sendIntInfo()
		if source == createHouseGUIcreate then
			local int_id, int_x, int_y, int_z = guiGetText(createHouseGUI.indid), guiGetText(createHouseGUI.x), guiGetText(createHouseGUI.y), guiGetText(createHouseGUI.z)
			triggerServerEvent("onAdminSendHouseIntInfo", getLocalPlayer(), int_id, int_x, int_y, int_z, price)
			showCursor(false)
			--outputDebugString(int_id.." "..int_x.." "..int_y.." "..int_z.."")
			destroyElement(createHouseGUI.window)
		end
	end
	addEventHandler("onClientGUIClick", getRootElement(), sendIntInfo)
end
--addEvent("onAdminCreateHouse", true)
--addEventHandler("onAdminCreateHouse", getRootElement(), createHouseGui)
addCommandHandler("createHouse", createHouseGui)