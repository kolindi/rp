houseGUI = {
    		label = {},
    		button = nil,
    		window = nil
}
DGS = exports.dgs
onHouse = false
local houseGUIOpen = false
function OpenHousePickupGui()
	if getElementData(source, "picType") == "house" and houseGUIOpen == false then
		--exports.functions:dxDrawTextOnElement(source, "ID: "..getElementData(housePickup, "id").."")
		houseGUI.window = DGS:dgsDxCreateWindow(0.72, 0.26, 0.28, 0.16, "Информация о недвижимости", true)
		
		houseGUI.label[1] = DGS:dgsDxCreateLabel(0.02, 0.26, 0.19, 0.10, "Стоимость: ", true, houseGUI.window)
		--DGS:dgsDxGUISetFont(houseGUI.label[1], "default-bold-small")
		houseGUI.label[2] = DGS:dgsDxCreateLabel(0.02, 0.39, 0.20, 0.10, "Номер: ", true, houseGUI.window)
		--DGS:dgsDxGUISetFont(houseGUI.label[2], "default-bold-small")
		houseGUI.label[3] = DGS:dgsDxCreateLabel(0.02, 0.52, 0.17, 0.10, "Интерьер: ", true, houseGUI.window)
		--DGS:dgsDxGUISetFont(houseGUI.label[3], "default-bold-small")
		houseGUI.price = DGS:dgsDxCreateLabel(0.28, 0.26, 0.28, 0.10, getElementData(source, "price"), true, houseGUI.window)
		houseGUI.id = DGS:dgsDxCreateLabel(0.28, 0.39, 0.28, 0.10, getElementData(source, "id"), true, houseGUI.window)
		houseGUI.int = DGS:dgsDxCreateLabel(0.28, 0.52, 0.28, 0.10, "", true, houseGUI.window)
		houseGUI.button = DGS:dgsDxCreateButton(0.67, 0.26, 0.28, 0.36, "Войти", true, houseGUI.window)
		DGS:dgsDxGUISetEnabled(houseGUI.button, false)
		if getElementData(getLocalPlayer(), "id") == getElementData(source, "Owner") or getElementData(source, "isOpen") == 1 then
			DGS:dgsDxGUISetEnabled(houseGUI.button, true)
		end
		local houseGUIOpen = true
		houseSource = source
		function EnterClientHouse(button, state)
			if state == "down" then
				if source == houseGUI.button then
					triggerServerEvent("onClientEnterHouse", localPlayer, houseSource)
					setElementData(localPlayer, "inHouse", 1, true)
 					--[[local enter_x, enter_y, enter_z = getElementPosition(getLocalPlayer())
					setElementInterior(getLocalPlayer(), getElementData(houseSource, "int_id"))
					setElementPosition(getLocalPlayer(), getElementData(houseSource, "int_x"), getElementData(houseSource, "int_y"), getElementData(houseSource, "int_z"))
					setElementDimension(getLocalPlayer(), getElementData(houseSource, "id") + 100)
					local exit_x, exit_y, exit_z = getElementPosition(getLocalPlayer())
					onHouse = true
				--local exitPickup = createPickup(exit_x, exit_y, exit_z, 3, 1318, 1)
					function createHouseExitPickup()
						local exitPickup = createPickup(exit_x, exit_y, exit_z, 3, 1318, 1)
						setElementDimension(exitPickup, getElementData(houseSource, "id") + 100)
						--killTimer(exitPickupTimer)
					end
					local exitPickupTimer = setTimer(createHouseExitPickup, 5000, 1)
					function ExitHouse(CommandName)
						if onHouse == true then
							setElementInterior(getLocalPlayer(), 0)
							setElementDimension(getLocalPlayer(), 0)
							setElementPosition(getLocalPlayer(), enter_x, enter_y, enter_z)
							destroyElement(exitPickup)
							onHouse = false
						end
					end
					--addEventHandler("onClientPickupHit", getRootElement(), ExitHouse)
					addCommandHandler("hExit", ExitHouse)]]
				end
			end
		end
		addEventHandler("onClientDgsDxMouseClick", getRootElement(), EnterClientHouse)
	end
end
addEventHandler("onClientPickupHit", getResourceRootElement(getThisResource()), OpenHousePickupGui)

function CloseHousePickupGui()
	if getElementData(source, "picType") == "house" then
		destroyElement(houseGUI.window)
		showCursor(false)
		local houseGUIOpen = false
	end
end
addEventHandler("onClientPickupLeave", getResourceRootElement(getThisResource()), CloseHousePickupGui)
--[[
function drawId()
	exports.functions:dxDrawTextOnElement(housePickup, "ID: "..getElementData(housePickup, "id").."")
end
addEventHandler("onClientRender", root, drawId)
]]