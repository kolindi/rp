database = call(getResourceFromName("db"), "db")
function taxesInit()
	local query = dbQuery(database, "SELECT * FROM taxes;")
	local result = dbPoll(query, -1)
	for _,row in ipairs(result) do
		local tax = createElement("tax")
		setElementData(tax, "id", tonumber(row['id']))
		setElementData(tax, "name", row['type'])
		setElementData(tax, "rusname", row['name'])
		setElementData(tax, "percent", tonumber(row['percent']))
		if row['perOrFix'] == "1" then
			setElementData(tax, "percentType", true)
			if getElementData(tax, "percent") > 100 then
				setElementData(tax, "percent", 0)
			end
		elseif row['perOrFix'] == "2" then
			setElementData(tax, "percentType", false)
		end
	end
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), taxesInit)