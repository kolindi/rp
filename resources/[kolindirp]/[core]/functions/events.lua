-- Система авторизации (старая)
--addEvent("createLoginPanel", true)
addEvent("onPlayerAuthLoad", true) -- Когда у игрока запустилась система авторизации

-- i18n
addEvent("changeLang", true)

-- Система времени
addEvent("payday", true) -- Вызывается для сервера каждый час.
addEvent("cPayday", true) -- Вызывается для каждого игрока каждый час

-- Хз
addEvent("onPlayerHouseSpawn", true) -- Когда игрок заспавнился в своём доме

-- Главное меню
addEvent("onPlayerChangeSerialCheck", true) -- Когда игрок включает/отключает проверку серийного номера

--Система уведомлений
addEvent("onPlayerGotNotification", true) -- Когда игрок получил уведомление

--Меню организаций
addEvent("onOfficerUvalPlayer", true) -- Когда игрок 9 или 10 ранга увольняет другого игрока
addEvent("onOfficerUpRankPlayer", true) -- Когда игрок 9 или 10 ранга повышает другого игрока
addEvent("onOfficerDownRankPlayer", true) -- Когда игрок 9 или 10 ранга понижает другого игрока
addEvent("onLeaderChangePayday", true) -- Когда лидер организации изменил зарплаты
addEvent("onOfficerPremPlayer", true) -- Когда игрок 9 или 10 ранга выдал премию члену организации
addEvent("onClientChangePaydayBankAccount", true) -- Когда игрок изменил номер банковского аккаунта для выплат

--Захват территорий
addEvent("onOrgAttackZone", true) -- Когда организация нападает на зону другой организации
addEvent("onPlayerInBattleZone", true) -- Когда игрок находится внутри зоны боя
addEvent("onPlayerBattleEnd", true) -- Когда бой закончился, а игрок находился в одной из воюющих фракций
addEvent("onServerUpdateBattleInfo", true) -- Когда сервер обновил информацию о бое

--LSPD
addEvent("onPlayerEquipmentMarkerHit", true) -- Когда игрок заходит на маркер подбора снаряжения
addEvent("onClientPoliceEquipmentClick", true) -- Когда игрок нажимает на какую-либо кнопку в меню снаряжения

--R Меню
addEvent("onClientEngineOnClick", true) -- Когда игрок нажимает на кнопку включения двигателя машины
addEvent("onClientEngineOffClick", true) -- Когда игрок нажимает на кнопку выключения двигателя машины
addEvent("onClientLightOnClick", true) -- Когда игрок нажимает на кнопку включения фар
addEvent("onClientLightOffClick", true) -- Когда игрок нажимает на кнопку отключения фар
addEvent("onClientCarOpenClick", true) -- Когда игрок нажимает на кнопку открытия своей машины
addEvent("onClientCarLockClick", true) -- Когда игрок нажимает на кнопку закрытия своей машины
addEvent("onClientExitHouseClick", true) -- Когда игрок нажимает на кнопку выхода из дома
addEvent("onClientHackCarClick", true) -- Когда игрок нажимает на кнопку взлома машины

--Система домов
addEvent("onClientEnterHouse", true) -- Когда игрок входит в дом

--
--addEvent("onPlayerChangeSerialCheck", true) -- Когда игрок нажимает на кнопку включения

--Создание банды
addEvent("onClientCreateGangButtonClick", true) -- Когда игрок нажимает на кнопку создания банды

--Админка
addEvent("onAdminGetAccInfo", true) -- Когда админ успешно вводит команду получения всех данных игрока
addEvent("onAdminCreateBusiness", true) -- Когда админ создаёт бизнес

--Система бизнесов
addEvent("onPlayerTalkOnNPC", true) -- Когда игрок наводится на NPC для разговора
	--Банк
	addEvent("onClientCheckBankBalance", true) -- Когда игрок проверяет баланс своего банквского аккаунта
	addEvent("successGetBankBalance", true) -- Когда игрок успешно проверяет баланс своего банковского аккаунта
	addEvent("onClientCreateBankAccount", true) -- Когда игрок создаёт банковский аккаунт
	addEvent("onClientInputMoneyBank", true) -- Когда игрок пополняет банковский счёт
	addEvent("onClientWithdrawMoneyBank", true) -- Когда игрок выводит деньги с банковского счёта
	addEvent("noMoneyOnBalance", true) -- Когда на счёте игрока меньше денег, чем он ввёл для снятия
	addEvent("noMoneyInBank", true) -- Когда на балансе банка меньше денег, чем хочет вывести игрок
	addEvent("successWithdrawBank", true) -- Когда игрок успешно вывел деньги со счёта в банке
-------

--Система инвентаря
addEvent("createInventory", true) -- Загрузка инвентаря игрока при входе

--Смартфон
addEvent("onClientAddContact", true)
addEvent("onClientOpenContacts", true)
addEvent("onClientOpenSmsApp", true)
addEvent("onClientOpenChat", true)
addEvent("onClientSendSms", true)
addEvent("onClientReadNewSms", true)
addEvent("onClientCreateChat", true)