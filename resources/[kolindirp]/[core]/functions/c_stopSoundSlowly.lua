function stopSoundSlowly (sElement)
    if not isElement(sElement) then return false end
    local sound_timer_quant = getSoundVolume(sElement)
    local slowlyStop = setTimer(
      function ()
        local soundVolume = getSoundVolume(sElement)
        setSoundVolume(sElement,soundVolume - 0.5)
	if soundVolume >= 0 then 
          stopSound(sElement) end
	end,400,sound_timer_quant*2
    )
end