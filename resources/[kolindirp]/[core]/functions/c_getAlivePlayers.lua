function getAlivePlayers()
  local alivePlayers = { }
  for i,p in ipairs (getElementsByType("player")) do
    if getElementHealth(p) > 0 then
      table.insert(alivePlayers,p)
    end 
  end
  return alivePlayers
end