function getOnlineAdmins()
	local t = {}
	for k,v in ipairs ( getElementsByType("player") ) do
		local acc = getPlayerAccount(v)
		if acc and not isGuestAccount(acc) then
			local accName = getAccountName(acc)
			local isAdmin = isObjectInACLGroup("user."..accName,aclGetGroup("Admin"))
			if isAdmin then
				table.insert(t,v)
			end
		end
	end
	return t
end