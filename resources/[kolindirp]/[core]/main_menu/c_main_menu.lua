DGS = exports.dgs
mmGUI = {
	window,
	button = {},
	label = {},
	edit = {},
	radiobutton = {}
}
mmOpen = false
serialWindowOpen = false
function triggerMainMenu(commandName)
	if mmOpen == false then
		mmOpen = true
		mmGUI.window = DGS:dgsDxCreateWindow(0.70, 0.25, 0.25, 0.40, "Главное меню", true)
		mmGUI.infobutton = DGS:dgsDxCreateButton(0.00, 0.00, 1, 0.10, "Информация о персонаже", true, mmGUI.window)
		mmGUI.settingsbutton = DGS:dgsDxCreateButton(0.00, 0.10, 1, 0.10, "Настройки аккаунта", true, mmGUI.window)
	end
end
addCommandHandler("menu", triggerMainMenu)
addCommandHandler("mn", triggerMainMenu)
addCommandHandler("mm", triggerMainMenu)

function buttonClicked()
	if source == mmGUI.infobutton then
		destroyMainButtons()
		mmGUI.namelabel = DGS:dgsDxCreateLabel(0.05, 0.05, 0.10, 0.10, "Имя_Фамилия: "..getPlayerName(getLocalPlayer()).."", true, mmGUI.window)
		mmGUI.idlabel = DGS:dgsDxCreateLabel(0.05, 0.12, 0.10, 0.10, "Номер аккаунта: "..getElementData(getLocalPlayer(), "id").."", true, mmGUI.window)
		if getElementData(localPlayer, "sex") == "1" then
			sex = "Мужской"
		else
			sex = "Женский"
		end
		mmGUI.sexlabel = DGS:dgsDxCreateLabel(0.05, 0.19, 0.10, 0.10, "Пол: "..tostring(sex).."", true, mmGUI.window)
		pOrg = getPlayerTeam(localPlayer)
		--mmGUI.org = DGS:dgsDxCreateLabel(0.05, 0.26, 0.10, 0.10, "Организация: "..getElementData(pOrg, "rusname").."", true, mmGUI.window)
		if getElementData(pOrg, "rusname") ~= false then
			mmGUI.org = DGS:dgsDxCreateLabel(0.05, 0.26, 0.10, 0.10, "Организация: "..getElementData(pOrg, "rusname").."", true, mmGUI.window)
		else
			mmGUI.org = DGS:dgsDxCreateLabel(0.05, 0.26, 0.10, 0.10, "Организация: "..getElementData(localPlayer, "org").."", true, mmGUI.window)
		end
		if getElementData(localPlayer, "org") ~= "Citizen" then
			mmGUI.rank = DGS:dgsDxCreateLabel(0.05, 0.33, 0.10, 0.10, "Должность: "..getElementData(localPlayer, "rank_name").."", true, mmGUI.window)
		end
	elseif source == mmGUI.settingsbutton then
		destroyMainButtons()
		mmGUI.serialcheckbutton = DGS:dgsDxCreateLabel(0.05, 0.02, 0.1, 0.1, "Проверка серийного номера:", true, mmGUI.window)
		if getElementData(getLocalPlayer(), "serialcheck") == 1 then
			mmGUI.serialcheckOn = DGS:dgsDxCreateButton(0.00, 0.10, 0.50, 0.07, "Включена", true, mmGUI.window, 0xFFFFFFFF, 1, 1, nil, nil, nil, 0xC814DB06, 0xC802730A, 0xC8325A)
			mmGUI.serialcheckOff = DGS:dgsDxCreateButton(0.50, 0.10, 0.50, 0.07, "Отключена", true, mmGUI.window, 0xFFFFFFFF, 1, 1, nil, nil, nil, 0x30280501, 0xC8720703, 0xC8325A)
		else
			mmGUI.serialcheckOn = DGS:dgsDxCreateButton(0.00, 0.10, 0.50, 0.07, "Включена", true, mmGUI.window, 0xFFFFFFFF, 1, 1, nil, nil, nil, 0x30036311, 0xC802730A, 0xC8325A)
			mmGUI.serialcheckOff = DGS:dgsDxCreateButton(0.50, 0.10, 0.50, 0.07, "Отключена", true, mmGUI.window, 0xFFFFFFFF, 1, 1, nil, nil, nil, 0xC8CF0B07, 0xC8720703, 0xC8325A)
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), buttonClicked)

function serialCheckChange()
	if source == mmGUI.serialcheckOn and getElementData(getLocalPlayer(), "serialcheck") == 1 then
		outputChatBox("Защита по серийному номеру уже включена.")
	elseif source == mmGUI.serialcheckOff and getElementData(getLocalPlayer(), "serialcheck") == 1 and serialWindowOpen == false then
		serialWindowOpen = true
		mmGUI.serialcheckWindow = DGS:dgsDxCreateWindow(0.50, 0.25, 0.40, 0.25, "Отключение проверки серийного номера", true)
		mmGUI.serialcheckWarning = DGS:dgsDxCreateLabel(0.05, 0.05, 0.10, 0.10, "ВНИМАНИЕ! После отключения проверки\n серийного номера на аккаунт можно будет\n войти с другого компьютера!", true, mmGUI.serialcheckWindow)
		mmGUI.serialcheckbuttonOff = DGS:dgsDxCreateButton(0.30, 0.50, 0.40, 0.20, "Отключить", true, mmGUI.serialcheckWindow)
	elseif source == mmGUI.serialcheckOff and getElementData(getLocalPlayer(), "serialcheck") == 0 then
		outputChatBox("Защита по серийному номеру и так отключена.")
	elseif source == mmGUI.serialcheckOn and getElementData(getLocalPlayer(), "serialcheck") == 0 and serialWindowOpen == false then
		serialWindowOpen = true
		mmGUI.serialcheckWindow = DGS:dgsDxCreateWindow(0.40, 0.25, 0.30, 0.25, "Включение проверки серийного номера", true)
		mmGUI.serialcheckWarning = DGS:dgsDxCreateLabel(0.05, 0.05, 0.10, 0.10, "ВНИМАНИЕ! После включения проверки\n серийного номера на аккаунт нельзя будет\n войти с другого компьютера!", true, mmGUI.serialcheckWindow)
		mmGUI.serialcheckbuttonOn = DGS:dgsDxCreateButton(0.30, 0.50, 0.40, 0.20, "Включить", true, mmGUI.serialcheckWindow)
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), serialCheckChange)

function triggerCheckChange()
	if source == mmGUI.serialcheckbuttonOff then
		triggerServerEvent("onPlayerChangeSerialCheck", getLocalPlayer(), 2, getPlayerName(getLocalPlayer()))
		destroyElement(mmGUI.serialcheckWindow)
		serialWindowOpen = false
		backButtonClicked()
	elseif source == mmGUI.serialcheckbuttonOn then
		triggerServerEvent("onPlayerChangeSerialCheck", getLocalPlayer(), 1, getPlayerName(getLocalPlayer()))
		destroyElement(mmGUI.serialcheckWindow)
		serialWindowOpen = false
		backButtonClicked()
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), triggerCheckChange)

function destroyMainButtons()
	mmGUI.backbutton = DGS:dgsDxCreateButton(0.00, 0.90, 1, 0.10, "Назад", true, mmGUI.window)
	destroyElement(mmGUI.infobutton)
	destroyElement(mmGUI.settingsbutton)
end

function backButtonClicked()
	if source == mmGUI.backbutton then
		mmOpen = false
		destroyElement(mmGUI.window)
		triggerMainMenu()
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), backButtonClicked)

function MainMenuClosed()
	if source == mmGUI.window then
		mmOpen = false
	elseif source == mmGUI.serialcheckWindow then
		serialWindowOpen = false
	end
end
addEventHandler("onClientDgsDxWindowClose", getRootElement(), MainMenuClosed)