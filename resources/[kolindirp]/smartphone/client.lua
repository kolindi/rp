DGS = exports.dgs
smartphoneOpen = false
smartGUI = {}

function openSmartphone()
	if smartphoneOpen == false then
		smartphoneOpen = true
		--smartGUI.win = DGS:dgsDxCreateWindow(0.6, 0.5, 0.21, 0.4, "Смартфон", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x10FFFFFF)
		smartGUI.win = DGS:dgsDxCreateImage(0.7, 0.4, 0.2, 0.6, ":smartphone/img/phone.png", true)
		smartGUI.desktop = DGS:dgsDxCreateImage(0.034, 0.079, 0.932, 0.817, ":smartphone/img/desktop.png", true, smartGUI.win)
		--DGS:dgsDxWindowSetSizable(smartGUI.win, false)
		createInfoBar()
		loadApps()
		showCursor(true)
	elseif smartphoneOpen == true then
		closeSmartphone()
	end
end

function createInfoBar()
	local time = getRealTime()
	local month = time.month + 1
	local year = 1900 + time.year
	if time.minute < 10 then
		time.minute = "0"..time.minute
	end
	if time.hour < 10 then
		time.hour = "0"..time.hour
	end
	if time.monthday < 10 then
		time.monthday = "0"..time.monthday
	end
	if month < 10 then
		month = "0"..month
	end
	smartGUI.info = DGS:dgsDxCreateEdit(0, 0, 1, 0.05, ""..time.monthday.."."..month.."."..year.." "..time.hour..":"..time.minute.."", true, smartGUI.desktop)
	DGS:dgsDxEditSetReadOnly(smartGUI.info, true)
end

function reloadInfoBar()
	destroyElement(smartGUI.info)
	createInfoBar()
end

addEventHandler("onClientResourceStart", resourceRoot,
	function()
		bindKey("N", "up", openSmartphone)
	end
)

function closeSmartphone()
	destroyElement(smartGUI.win)
	smartphoneOpen = false
	showCursor(false)
	x = 0.1
	y = 0.15
	chatY = 0.05
	smsY = 0.05
	if smartGUI.contacts.callButton then
		destroyElement(smartGUI.contacts.callButton)
		destroyElement(smartGUI.contacts.smsButton)
		destroyElement(smartGUI.contacts.delButton)
	end
end

x = 0.1
y = 0.15

smartGUI.apps = {}
function addAppToDesktop(name, text, logo, logoBlue)
	smartGUI.apps[name] = smartGUI.apps[name] or {}
	--smartGUI.apps[name].logo = DGS:dgsDxCreateImage(x, y, 0.1, 0.1, logo, true, smartGUI.win)
	--smartGUI.apps[name].text = DGS:dgsDxCreateLabel(x, y+0.1, 0.1, 0.05, text, true, smartGUI.win)
	smartGUI.apps[name] = DGS:dgsDxCreateButton(x, y, 0.15, 0.15, "\n\n\n\n\n"..text, true, smartGUI.desktop, 0xFFFFFFFF, 1, 1, logo, logoBlue, logo)
	DGS:dgsDxGUISetProperty(smartGUI.apps[name], "name", name)
	DGS:dgsDxGUISetProperty(smartGUI.apps[name], "type", "app_button")
	x = x + 0.3
	if x > 0.71 then
		x = 0.1
		y = y + 0.2
	end
end

function clickTrigger(button, state)
	if button == "left" and state == "up" then
		if DGS:dgsDxGUIGetProperty(source, "type") == "app_button" then
			openApp(DGS:dgsDxGUIGetProperty(source, "name"))
		elseif source == smartGUI.backButton then
			closeSmartphone()
			openSmartphone()
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), clickTrigger)

function openApp(name)
	DGS:dgsDxImageSetImage(smartGUI.desktop, dxCreateTexture(":smartphone/img/white.png"))
	for _,appLogo in pairs(smartGUI.apps) do
		destroyElement(appLogo)
	end
	smartGUI.backButton = DGS:dgsDxCreateButton(0, 0.9, 1, 0.1, "На рабочий стол", true, smartGUI.desktop)
	if name == "app_phone" then
		openPhone()
	elseif name == "app_sms" then
		openSms()
	elseif name == "app_contacts" then
		openContacts()
	end
end