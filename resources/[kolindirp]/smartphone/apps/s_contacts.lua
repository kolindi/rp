function saveContact(number, name)
	local pId = getElementData(source, "id")
	local query = dbQuery(db, "SELECT number FROM phone_contacts WHERE pId = '"..pId.."' AND number = '"..tonumber(number).."';")
	local result, num_rows = dbPoll(query, -1)
	if num_rows < 1 then
		local query = dbExec(db, "INSERT INTO phone_contacts (pId, number, name) VALUES ('"..pId.."', '"..tonumber(number).."', '"..name.."');")
	else
		local query = dbExec(db, "UPDATE phone_contacts SET name = '"..name.."' WHERE pId = '"..pId.."' AND number = '"..tonumber(number).."';")
	end
end
addEventHandler("onClientAddContact", getRootElement(), saveContact)

function getContacts(isContactsApp)
	local pId = getElementData(source, "id")
	local query = dbQuery(db, "SELECT * FROM phone_contacts WHERE pId = '"..pId.."';")
	local result = dbPoll(query, -1)
	for _,data in ipairs(result) do
		triggerClientEvent(source, "onGetContact", source,
			data['id'],
			data['number'],
			data['name'],
			isContactsApp
		)
	end
end
addEventHandler("onClientOpenContacts", getRootElement(), getContacts)