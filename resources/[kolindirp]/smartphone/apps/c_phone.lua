function initPhone()
	addApp("app_phone", "Телефон", ":smartphone/apps/img/phone.png", ":smartphone/apps/img/phone_blue.png")
end
addEventHandler("onClientOpenSmartphone", localPlayer, initPhone)

smartGUI.phone = {}

function openPhone(addContact, sms)
	smartGUI.phone.Edit = DGS:dgsDxCreateEdit(0.1, 0.075, 0.5, 0.1, "", true, smartGUI.desktop, 0xFF000000, 2, 2)
	DGS:dgsDxEditSetReadOnly(smartGUI.phone.Edit, true)
	DGS:dgsDxEditSetMaxLength(smartGUI.phone.Edit, 6)
	smartGUI.phone.deleteButton = DGS:dgsDxCreateButton(0.7, 0.075, 0.2, 0.1, "X", true, smartGUI.desktop)
	smartGUI.phone.Numb = smartGUI.phone.Numb or {}
	smartGUI.phone.Numb[1] = DGS:dgsDxCreateButton(0.1, 0.2, 0.2, 0.15, "1", true, smartGUI.desktop)
	smartGUI.phone.Numb[2] = DGS:dgsDxCreateButton(0.4, 0.2, 0.2, 0.15, "2", true, smartGUI.desktop)
	smartGUI.phone.Numb[3] = DGS:dgsDxCreateButton(0.7, 0.2, 0.2, 0.15, "3", true, smartGUI.desktop)
	smartGUI.phone.Numb[4] = DGS:dgsDxCreateButton(0.1, 0.4, 0.2, 0.15, "4", true, smartGUI.desktop)
	smartGUI.phone.Numb[5] = DGS:dgsDxCreateButton(0.4, 0.4, 0.2, 0.15, "5", true, smartGUI.desktop)
	smartGUI.phone.Numb[6] = DGS:dgsDxCreateButton(0.7, 0.4, 0.2, 0.15, "6", true, smartGUI.desktop)
	smartGUI.phone.Numb[7] = DGS:dgsDxCreateButton(0.1, 0.6, 0.2, 0.15, "7", true, smartGUI.desktop)
	smartGUI.phone.Numb[8] = DGS:dgsDxCreateButton(0.4, 0.6, 0.2, 0.15, "8", true, smartGUI.desktop)
	smartGUI.phone.Numb[9] = DGS:dgsDxCreateButton(0.7, 0.6, 0.2, 0.15, "9", true, smartGUI.desktop)
	DGS:dgsDxGUISetProperty(smartGUI.phone.Numb[1], "numb", "1")
	DGS:dgsDxGUISetProperty(smartGUI.phone.Numb[2], "numb", "2")
	DGS:dgsDxGUISetProperty(smartGUI.phone.Numb[3], "numb", "3")
	DGS:dgsDxGUISetProperty(smartGUI.phone.Numb[4], "numb", "4")
	DGS:dgsDxGUISetProperty(smartGUI.phone.Numb[5], "numb", "5")
	DGS:dgsDxGUISetProperty(smartGUI.phone.Numb[6], "numb", "6")
	DGS:dgsDxGUISetProperty(smartGUI.phone.Numb[7], "numb", "7")
	DGS:dgsDxGUISetProperty(smartGUI.phone.Numb[8], "numb", "8")
	DGS:dgsDxGUISetProperty(smartGUI.phone.Numb[9], "numb", "9")
	smartGUI.phone.Call = DGS:dgsDxCreateButton(0.1, 0.78, 0.8, 0.1, "Подтвердить", true, smartGUI.desktop)
	--smartGUI.phone.Sms = DGS:dgsDxCreateButton(0.6, 0.78, 0.3, 0.1, "SMS", true, smartGUI.desktop)
end

function phoneClickTrigger(button, state)
	if button == "left" and state == "up" and source then
		local numb = DGS:dgsDxGUIGetProperty(source, "numb")
		if numb then
			local text = DGS:dgsDxGUIGetText(smartGUI.phone.Edit)
			if string.len(text) > 5 then
				return
			else
				DGS:dgsDxGUISetText(smartGUI.phone.Edit, text..numb)
			end
		end
		if source == smartGUI.phone.deleteButton then
			local text = DGS:dgsDxGUIGetText(smartGUI.phone.Edit)
			local newText = string.sub(text, 0, string.len(text) - 1)
			DGS:dgsDxGUISetText(smartGUI.phone.Edit, newText)
		elseif source == smartGUI.phone.Call then
			number = DGS:dgsDxGUIGetText(smartGUI.phone.Edit)
			if number == "8888" then
				triggerEvent("onClientCallToTaxi", localPlayer)
				phoneGUIDestroy()
				phoneNumbDestroy()
				createCallGui()
			else
				if string.len(number) ~= 6 then
					createNotification("Введён неверный номер телефона!")
				else
					phoneNumbDestroy()
					phoneGUIDestroy()
					smartGUI.phone.NumbLabel = DGS:dgsDxCreateLabel(0.1, 0.2, 0.8, 0.1, "Выберите действие с номером\n"..number..":", true, smartGUI.desktop, 0xFF000000)
					smartGUI.phone.CallButton = DGS:dgsDxCreateButton(0.1, 0.3, 0.8, 0.1, "Вызов", true, smartGUI.desktop)
					smartGUI.phone.SmsButton = DGS:dgsDxCreateButton(0.1, 0.45, 0.8, 0.1, "Создать чат", true, smartGUI.desktop)
					smartGUI.phone.AddContactButton = DGS:dgsDxCreateButton(0.1, 0.60, 0.8, 0.1, "Добавить в контакты", true, smartGUI.desktop)
				end
			end
		elseif source == smartGUI.phone.SmsButton then
			phoneGUIDestroy()
			--openSmsWindow(number)
			triggerServerEvent("onClientCreateChat", localPlayer, number)
			closeSmartphone()
			openSmartphone()
			createNotification("Чат успешно создан!")
		elseif source == smartGUI.phone.AddContactButton then
			phoneGUIDestroy()
			addContact(number)
		elseif source == smartGUI.phone.CallButton then
			phoneGUIDestroy()
			phoneNumbDestroy()
			createCallGui()
		elseif source == smartGUI.phone.dropCallButton then
			closeSmartphone()
			openSmartphone()
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), phoneClickTrigger)

function phoneGUIDestroy()
	for _,phoneGUI in pairs(smartGUI.phone) do
		if isElement(phoneGUI) then
			destroyElement(phoneGUI)
		end
	end
end

function phoneNumbDestroy()
	for _,phoneGUI in pairs(smartGUI.phone.Numb) do
		destroyElement(phoneGUI)
	end
end

function createCallGui()
	smartGUI.phone.dropCallButton = DGS:dgsDxCreateButton(0.4, 0.65, 0.2, 0.2, "", true, smartGUI.desktop, 0xFF000000, 1, 1, ":smartphone/apps/img/dropCall.png", ":smartphone/apps/img/dropCall_onMouse.png", ":smartphone/apps/img/dropCall.png", 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF)
end