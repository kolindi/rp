function initContacts()
	addApp("app_contacts", "Контакты", ":smartphone/apps/img/contacts.png", ":smartphone/apps/img/contacts_blue.png")
	triggerServerEvent("onClientOpenContacts", localPlayer)
end
addEventHandler("onClientOpenSmartphone", localPlayer, initContacts)

smartGUI.contacts = smartGUI.contacts or {}

function openContacts()
	smartGUI.contacts.addContactButton = DGS:dgsDxCreateButton(0, 0.798, 1, 0.1, "Добавить контакт", true, smartGUI.desktop)
	smartGUI.contacts.grid = DGS:dgsDxCreateGridList(0, 0.05, 1, 0.75, true, smartGUI.desktop)
	smartGUI.contacts.gridName = DGS:dgsDxGridListAddColumn(smartGUI.contacts.grid, "Имя", 0.45)
	smartGUI.contacts.gridNumber = DGS:dgsDxGridListAddColumn(smartGUI.contacts.grid, "Номер", 0.3)
	triggerServerEvent("onClientOpenContacts", localPlayer, true)
end

function addContact(number)
	smartGUI.contacts.addContactLabel = DGS:dgsDxCreateLabel(0.05, 0.1, 1, 0.1, "Введите имя для номера "..number..":", true, smartGUI.desktop, 0xFF000000)
	smartGUI.contacts.name = DGS:dgsDxCreateEdit(0.05, 0.3, 0.9, 0.1, "", true, smartGUI.desktop)
	smartGUI.contacts.saveContactButton = DGS:dgsDxCreateButton(0, 0.798, 1, 0.1, "Сохранить контакт", true, smartGUI.desktop)
end

function setContacts(id, number, name, isContactsApp)
	if isContactsApp then
		local row = DGS:dgsDxGridListAddRow(smartGUI.contacts.grid)
		DGS:dgsDxGridListSetItemText(smartGUI.contacts.grid, row, smartGUI.contacts.gridName, name)
		DGS:dgsDxGridListSetItemText(smartGUI.contacts.grid, row, smartGUI.contacts.gridNumber, number)
	end
	smartGUI.contacts.contact = smartGUI.contacts.contact or {}
	smartGUI.contacts.contact[number] = {
		name = name
	}
end
addEvent("onGetContact", true)
addEventHandler("onGetContact", getRootElement(), setContacts)

function contactsClickTrigger(button, state)
	if button == "left" and state == "up" then
		if source == smartGUI.contacts.addContactButton then
			closeSmartphone()
			openSmartphone()
			openApp("app_phone")
		elseif source == smartGUI.contacts.saveContactButton then
			local contactName = DGS:dgsDxGUIGetText(smartGUI.contacts.name)
			if contactName ~= "" then
				triggerServerEvent("onClientAddContact", localPlayer, number, contactName)
				closeSmartphone()
				openSmartphone()
				createNotification("Контакт сохранён!")
			else
				createNotification("Введите имя контакта!")
			end
		elseif source == smartGUI.contacts.grid then
			if smartGUI.contacts.callButton then
				destroyElement(smartGUI.contacts.callButton)
				destroyElement(smartGUI.contacts.smsButton)
				destroyElement(smartGUI.contacts.delButton)
			end
			local selected = DGS:dgsDxGridListGetSelectedItem(smartGUI.contacts.grid)
			if selected ~= -1 then
				local x,y = getCursorPosition()
				smartGUI.contacts.callButton = DGS:dgsDxCreateButton(x, y, 0.05, 0.04, "Вызов", true)
				smartGUI.contacts.smsButton = DGS:dgsDxCreateButton(x, y+0.04, 0.05, 0.04, "SMS", true)
				smartGUI.contacts.delButton = DGS:dgsDxCreateButton(x, y+0.08, 0.05, 0.04, "Удалить", true)
				DGS:dgsDxGUIBringToFront(smartGUI.contacts.callButton)
				DGS:dgsDxGUIBringToFront(smartGUI.contacts.smsButton)
				DGS:dgsDxGUIBringToFront(smartGUI.contacts.delButton)
			end
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), contactsClickTrigger)