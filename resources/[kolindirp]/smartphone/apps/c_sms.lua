function initSms()
	addApp("app_sms", "Сообщения", ":smartphone/apps/img/sms.png", ":smartphone/apps/img/sms_blue.png")
end
addEventHandler("onClientOpenSmartphone", localPlayer, initSms)

smartGUI.sms = {}
chatId = nil

function openSmsWindow(chatId)
	chatID = tonumber(chatId)
	for _,smsGUI in pairs(smartGUI.sms) do
		if isElement(smsGUI) then
			destroyElement(smsGUI)
		end
	end
	smartGUI.sms.chatScroll = DGS:dgsDxCreateScrollPane(0, 0.05, 1, 0.70, true, smartGUI.desktop)
	--DGS:dgsDxGUISetProperty(smartGUI.)
	smartGUI.sms.chatEdit = DGS:dgsDxCreateEdit(0.01, 0.82, 0.8, 0.07, "", true, smartGUI.desktop)
	DGS:dgsDxEditSetMaxLength(smartGUI.sms.chatEdit, 27)
	smartGUI.sms.chatSendButton = DGS:dgsDxCreateButton(0.82, 0.82, 0.15, 0.07, ">", true, smartGUI.desktop)
end

chatY = 0.05

function addSms(fromNumber, text, id)
	smartGUI.sms.sms = smartGUI.sms.sms or {}
	if fromNumber == getElementData(localPlayer, "number") then
		smartGUI.sms.sms[id] = DGS:dgsDxCreateEdit(0.05, chatY, 0.9, 0.1, text, true, smartGUI.sms.chatScroll, 0xFF000000, 1, 1, nil, 0xFF51C0FF)
	else
		smartGUI.sms.sms[id] = DGS:dgsDxCreateEdit(0, chatY, 0.9, 0.1, text, true, smartGUI.sms.chatScroll, 0xFF000000)
	end
	DGS:dgsDxEditSetReadOnly(smartGUI.sms.sms[id], true)
	chatY = chatY + 0.15
end
addEvent("onGetSms", true)
addEventHandler("onGetSms", getRootElement(), addSms)

function openSms()
	smartGUI.sms.openSmsWindowButton = DGS:dgsDxCreateButton(0, 0.798, 1, 0.1, "Создать чат", true, smartGUI.desktop)
	smartGUI.sms.smsLabel = DGS:dgsDxCreateLabel(0.05, 0.05, 0.99, 0.05, "Выберите или создайте чат", true, smartGUI.desktop, 0xFF000000)
	smartGUI.sms.scroll = DGS:dgsDxCreateScrollPane(0, 0.1, 1, 0.70, true, smartGUI.desktop)
	triggerServerEvent("onClientOpenSmsApp", localPlayer)
	--[[for i = 1, 10 do
		-- create a label with their name on the scrollpane
		DGS:dgsDxCreateLabel(0.1,i*0.1,0.3,0.05,i,true,smartGUI.sms.scroll, 0xFF000000)
	end]]
end

smsY = 0.05
smartGUI.sms.contactsInApp = smartGUI.contactsInApp or {}
smartGUI.sms.smsButton = smartGUI.sms.smsButton or {}

function addContactToSmsApp(id, contactNumber, isHaveNewSms)
	--smartGUI.sms.contactsInApp = smartGUI.contactsInApp or {}
	smartGUI.sms.contactsInApp[id] = contact
	if isHaveNewSms == 1 then
		smartGUI.sms.smsButton[id] = DGS:dgsDxCreateButton(0.05, smsY, 0.8, 0.1, contactNumber, true, smartGUI.sms.scroll, 0xFFFFFFFF, 1, 1, nil, nil, nil, 0xFF040075)
	else
		smartGUI.sms.smsButton[id] = DGS:dgsDxCreateButton(0.05, smsY, 0.8, 0.1, contactNumber, true, smartGUI.sms.scroll)
	end
	DGS:dgsDxGUISetProperty(smartGUI.sms.smsButton[id], "chatId", tostring(id))
	DGS:dgsDxGUISetProperty(smartGUI.sms.smsButton[id], "contactNumber", tostring(contactNumber))
	DGS:dgsDxGUISetProperty(smartGUI.sms.smsButton[id], "newSms", tostring(isHaveNewSms))
	smsY = smsY + 0.11
	if smartGUI.contacts.contact[contactNumber] then
		DGS:dgsDxGUISetText(smartGUI.sms.smsButton[id], smartGUI.contacts.contact[contactNumber].name)
	end
	--smartGUI.sms.smsContactButton = DGS:dgsDxCreateButton(0, 0, 0.9, 0.1, )
end
addEvent("onGetChats", true)
addEventHandler("onGetChats", getRootElement(), addContactToSmsApp)

function smsNotif(newSmsChat, newSmsText)
	--outputChatBox("Текущий чат: "..chatID)
	--outputChatBox("Чат с новым смс: "..newSmsChat)
	if smartGUI.sms.chatScroll then
		if newSmsChat == chatID then
			addSms(getTickCount(), newSmsText, getTickCount())
		else
			playSound(":smartphone/apps/sound/sms.mp3")
		end
	end
end
addEvent("onGetNewSms", true)
addEventHandler("onGetNewSms", getRootElement(), smsNotif)

function smsClickTrigger(button, state)
	if button == "left" and state == "up" and source then
		chatIdClick = tonumber(DGS:dgsDxGUIGetProperty(source, "chatId"))
		if chatIdClick then
			--chatId = tonumber(DGS:dgsDxGUIGetProperty(source, "chatId"))
			contactNumber = tonumber(DGS:dgsDxGUIGetProperty(source, "contactNumber"))
			local newSmsNotif = tonumber(DGS:dgsDxGUIGetProperty(source, "newSms"))
			if newSmsNotif == 1 then
				triggerServerEvent("onClientReadNewSms", localPlayer, chatIdClick)
			end
			triggerServerEvent("onClientOpenChat", localPlayer, chatIdClick)
			openSmsWindow(chatIdClick)
		end
		if source == smartGUI.sms.openSmsWindowButton then
			closeSmartphone()
			openSmartphone()
			openApp("app_phone")
		elseif source == smartGUI.sms.chatSendButton then
			local smsText = DGS:dgsDxGUIGetText(smartGUI.sms.chatEdit)
			if smsText ~= "" then
				addSms(getElementData(localPlayer, "number"), smsText, getTickCount())
				triggerServerEvent("onClientSendSms", localPlayer, smsText, chatID, contactNumber)
				DGS:dgsDxGUISetText(smartGUI.sms.chatEdit, "")
			end
		end
	end
end
addEventHandler("onClientDgsDxMouseClick", getRootElement(), smsClickTrigger)