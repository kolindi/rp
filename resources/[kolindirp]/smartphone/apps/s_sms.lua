function getChats()
	local number = getElementData(source, "number")
	local query = dbQuery(db, "SELECT * FROM phone_chats WHERE numberOne = '"..number.."' OR numberTwo = '"..number.."';")
	local result = dbPoll(query, -1)
	for _,chat in ipairs(result) do
		if tonumber(chat['numberOne']) == number then
			contact = tonumber(chat['numberTwo'])
			smsNotif = tonumber(chat['numberOneNotif'])
		else
			contact = tonumber(chat['numberOne'])
			smsNotif = tonumber(chat['numberTwoNotif'])
		end
		triggerClientEvent(source, "onGetChats", source,
			tonumber(chat['id']),
			contact,
			smsNotif
		)
	end
end
addEventHandler("onClientOpenSmsApp", getRootElement(), getChats)

function getSms(chatId)
	local query = dbQuery(db, "SELECT * FROM phone_sms WHERE chat = '"..tonumber(chatId).."';")
	local result = dbPoll(query, -1)
	for _,sms in ipairs(result) do
		triggerClientEvent(source, "onGetSms", source,
			tonumber(sms['fromNumber']),
			sms['text'],
			tonumber(sms['id'])
		)
	end
end
addEventHandler("onClientOpenChat", getRootElement(), getSms)

function createSms(smsText, chatId, toNumber)
	local query = dbExec(db, "INSERT INTO phone_sms (chat, fromNumber, text) VALUES ('"..tonumber(chatId).."', '"..getElementData(source, "number").."', '"..smsText.."');")
	local query = dbExec(db, "UPDATE phone_chats SET numberOneNotif = '"..tonumber(1).."' WHERE id = '"..tonumber(chatId).."' AND numberOne = '"..toNumber.."';")
	local query = dbExec(db, "UPDATE phone_chats SET numberTwoNotif = '"..tonumber(1).."' WHERE id = '"..tonumber(chatId).."' AND numberTwo = '"..toNumber.."';")
	local players = getElementsByType("player")
	for _,playerSource in ipairs(players) do
		if getElementData(playerSource, "number") == toNumber then
			triggerClientEvent(playerSource, "onGetNewSms", playerSource, chatId, smsText)
		end
	end
end
addEventHandler("onClientSendSms", getRootElement(), createSms)

function offSmsNotif(chatId)
	local toNumber = getElementData(source, "number")
	local query = dbExec(db, "UPDATE phone_chats SET numberOneNotif = '"..tonumber(0).."' WHERE id = '"..tonumber(chatId).."' AND numberOne = '"..toNumber.."';")
	local query = dbExec(db, "UPDATE phone_chats SET numberTwoNotif = '"..tonumber(0).."' WHERE id = '"..tonumber(chatId).."' AND numberTwo = '"..toNumber.."';")
end
addEventHandler("onClientReadNewSms", getRootElement(), offSmsNotif)

function createChat(numberTwo)
	local query = dbExec(db, "INSERT INTO phone_chats (numberOne, numberTwo) VALUES ('"..getElementData(source, "number").."', '"..tonumber(numberTwo).."');")
end
addEventHandler("onClientCreateChat", getRootElement(), createChat)