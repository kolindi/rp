function createNotification(text)
	DGS:dgsDxGUISetText(smartGUI.info, text)
	setTimer(function()
		reloadInfoBar()
	end, 3000, 1)
end

function smsNotif()
	playSound(":smartphone/apps/sound/sms.mp3")
end
addEvent("onGetNewSms", true)
addEventHandler("onGetNewSms", getRootElement(), smsNotif)