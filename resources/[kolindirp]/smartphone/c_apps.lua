addEvent("onClientOpenSmartphone", true)

apps = {}
function loadApps()
	triggerEvent("onClientOpenSmartphone", localPlayer)
end

function addApp(name, text, logo, logoBlue)
	apps.name = {
		text = text,
		logo = logo,
		logoBlue = logoBlue
	}
	addAppToDesktop(name, text, logo, logoBlue)
end