database = call(getResourceFromName("db"), "db")
-- source - Команда, которая начала атаку.
-- by - Член команды, который начал атаку.
-- defensiveTeam - Команда, которая обороняется.
-- zone - Сама зона, за которую начат бой.
function zoneBattleInit(by, defensiveTeam, zone)
	if getElementData(zone, "battle") == false then
		zone = zone
		attackTeam = source
		defensiveTeam = defensiveTeam
		attackPlayers = getPlayersInTeam(source)
		defensivePlayers = getPlayersInTeam(defensiveTeam)
		zoneName = getElementData(zone, "name")
		zoneId = getElementData(zone, "id")
		if getElementData(source, "rusname") ~= false then
			zoneOwnerName = getElementData(source, "rusname")
		else
			zoneOwnerName = getTeamName(source)
		end
		if getElementData(defensiveTeam, "rusname") ~= false then
			zoneOwnerName = getElementData(defensiveTeam, "rusname")
		else
			zoneOwnerName = getTeamName(defensiveTeam)
		end
		setElementData(zone, "battle", true)
		setElementData(zone, "attack", source)
		setElementData(zone, "defend", defensiveTeam)
		setElementData(source, "attack", true)
		--players = getElementsByType("player")
		--attackInZone = 0
		for _,playerSource in ipairs(attackPlayers) do
			exports.ns:sendNotification(playerSource, "battle", "Ваша фракция напала на территорию "..zoneName..", которой владеет "..zoneOwnerName.."!")
			if getElementData(playerSource, "zone") == zone then
				triggerClientEvent(playerSource, "onPlayerInBattleZone", playerSource, zoneName, getTeamName(source), zoneOwnerName)
				--attackInZone = attackInZone + 1
				updateBattleInfo("+", 1, 0)
			end
		end
		for _,playerSource in ipairs(defensivePlayers) do
			exports.ns:sendNotification(playerSource, "battle", "На вашу территорию "..zoneName.." напала "..getTeamName(source).."!")
			if getElementData(playerSource, "zone") == zone then
				triggerClientEvent(playerSource, "onPlayerInBattleZone", playerSource, zoneName, getTeamName(source), zoneOwnerName)
				updateBattleInfo("+", 0, 1)
			end
		end
		updateTimer = setTimer(calculatePercent, 1000, 0, true, source, defensiveTeam, zone)
		radarArea = getElementData(zone, "radarArea")
		setRadarAreaFlashing(radarArea, true)
	end
end
addEventHandler("onOrgAttackZone", getRootElement(), zoneBattleInit)

attack = 0
defend = 0

function updateBattleInfo(plusMinus, attackChange, defendChange)
	if plusMinus == "+" then
		if attackChange ~= 0 then
			attack = attack + attackChange
		end
		if defendChange ~= 0 then
			defend = defend + defendChange
		end
	elseif plusMinus == "-" then
		if attackChange ~= 0 then
			attack = attack - attackChange
		end
		if defendChange ~= 0 then
			defend = defend - defendChange
		end
	end
end

battleTimeSecond = 0

function calculatePercent(addToBattleTime, attackTeam, defensiveTeam, zone)
	if addToBattleTime == true then
		battleTimeSecond = battleTimeSecond + 1
	end
	totalPlayers = attack + defend
	attackCoeff = totalPlayers / attack
	attackPercent = 100 / attackCoeff
	defendCoeff = totalPlayers / defend
	defendPercent = 100 / defend
	if attackPercent < 0 or attackPercent > 100 then
		attackPercent = 0
	end
	if defendPercent < 0 or defendPercent > 100 then
		defendPercent = 0
	end
	if battleTimeSecond > 10 then
		outputChatBox("Конец времени захвата")
		if attackPercent > 79 then
			outputChatBox("Победили атакующие")
			winner(attackTeam, attackTeam, defensiveTeam, zone)
		elseif defendPercent > 79 then
			outputChatBox("Победила оборона")
			winner(defensiveTeam, attackTeam, defensiveTeam, zone)
		end
	end
	local attackPl = getPlayersInTeam(attackTeam)
	for _,playerSource in ipairs(attackPl) do
		if getElementData(playerSource, "zone") == zone then
			triggerClientEvent(playerSource, "onServerUpdateBattleInfo", playerSource, attackPercent, defendPercent, battleTimeSecond)
		end
	end
	local defendPl = getPlayersInTeam(defensiveTeam)
	for _,playerSource in ipairs(defensivePlayers) do
		if getElementData(playerSource, "zone") == zone then
			triggerClientEvent(playerSource, "onServerUpdateBattleInfo", playerSource, attackPercent, defendPercent, battleTimeSecond)
		end
	end
end

function winner(winnerTeam, attackTeam, defensiveTeam, zone)
	if winnerTeam == attackTeam then
		killTimer(updateTimer)
		local query = dbExec(database, "UPDATE zone SET owner = '"..getTeamName(attackTeam).."' WHERE id = '"..zoneId.."';")
		local red = getElementData(attackTeam, "red")
		local green = getElementData(attackTeam, "green")
		local blue = getElementData(attackTeam, "blue")
		setRadarAreaColor(radarArea, red, green, blue, 100)
		setRadarAreaFlashing(radarArea, false)
		setElementData(zone, "battle", false)
		setElementData(zone, "attack", nil)
		setElementData(zone, "defend", nil)
		for _,playerSource in ipairs(getPlayersInTeam(attackTeam)) do
			exports.ns:sendNotification(playerSource, "battleSuccess", "Вы успешно захватили территорию "..zoneName.."!")
			triggerClientEvent(playerSource, "onPlayerBattleEnd", playerSource)
		end
		for _,playerSource in ipairs(getPlayersInTeam(defensiveTeam)) do
			exports.ns:sendNotification(playerSource, "battleFail", "Вы потеряли контроль над территорией "..zoneName.."!")
			triggerClientEvent(playerSource, "onPlayerBattleEnd", playerSource)
		end
	elseif winnerTeam == defensiveTeam then
		setRadarAreaFlashing(radarArea, false)
		setElementData(zone, "battle", false)
		for _,playerSource in ipairs(getPlayersInTeam(attackTeam)) do
			exports.ns:sendNotification(playerSource, "battleSuccess", "Вы не смогли захватить территорию "..zoneName.."!")
			triggerClientEvent(playerSource, "onPlayerBattleEnd", playerSource)
		end
		for _,playerSource in ipairs(getPlayersInTeam(defensiveTeam)) do
			exports.ns:sendNotification(playerSource, "battleFail", "Вы сохранили контроль над территорией "..zoneName.."!")
			triggerClientEvent(playerSource, "onPlayerBattleEnd", playerSource)
		end
	end
end

function playerEnterBattleZone(hitElement, dimension)
	if getElementType(hitElement) == "player" and dimension == true then
		if getElementData(source, "battle") == true then
			playerTeam = getPlayerTeam(hitElement)
			if playerTeam == attackTeam then
				updateBattleInfo("+", 1, 0)
				triggerClientEvent(hitElement, "onPlayerInBattleZone", hitElement, zoneName, getTeamName(attackTeam), zoneOwnerName)
			elseif playerTeam == defensiveTeam then
				updateBattleInfo("+", 0, 1)
				triggerClientEvent(hitElement, "onPlayerInBattleZone", hitElement, zoneName, getTeamName(attackTeam), zoneOwnerName)
			end
		end
	end
end
addEventHandler("onColShapeHit", getRootElement(), playerEnterBattleZone)

function playerLeaveBattleZone(hitElement, dimension)
	if getElementType(hitElement) == "player" and dimension == true then
		if getElementData(source, "battle") == true then
			playerTeam = getPlayerTeam(hitElement)
			if playerTeam == attackTeam then
				updateBattleInfo("-", 1, 0)
				triggerClientEvent(hitElement, "onPlayerBattleEnd", hitElement)
			elseif playerTeam == defensiveTeam then
				updateBattleInfo("-", 0, 1)
				triggerClientEvent(hitElement, "onPlayerBattleEnd", hitElement)
			end
		end
	end
end
addEventHandler("onColShapeLeave", getRootElement(), playerLeaveBattleZone)

function playerDeadInBattleZone()
	if getElementData(source, "zone") == zone then
		playerTeam = getPlayerTeam(source)
		if playerTeam == attackTeam then
			updateBattleInfo("-", 1, 0)
			triggerClientEvent(source, "onPlayerBattleEnd", source)
		elseif playerTeam == defensiveTeam then
			updateBattleInfo("-", 0, 1)
			triggerClientEvent(source, "onPlayerBattleEnd", source)
		end
	end
end
addEventHandler("onPlayerWasted", getRootElement(), playerDeadInBattleZone)