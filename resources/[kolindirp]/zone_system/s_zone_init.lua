database = call(getResourceFromName("db"), "getDb")
function zoneInit()
	local query = dbQuery(database, "SELECT * FROM zone;")
	local result = dbPoll(query, -1)
	for _, zone in ipairs(result) do
		local ownerTeam = getTeamFromName(zone['owner'])
		local radarArea = createRadarArea(zone['leftX'], zone['bottomY'], zone['sizeX'], zone['sizeY'], tonumber(getElementData(ownerTeam, "red")), tonumber(getElementData(ownerTeam, "green")), tonumber(getElementData(ownerTeam, "blue")), 100)
		local colision = createColRectangle(zone['leftX'], zone['bottomY'], zone['sizeX'], zone['sizeY'])
		setElementData(colision, "radarArea", radarArea)
		setElementData(colision, "id", tonumber(zone['id']))
		setElementData(colision, "owner", ownerTeam)
		setElementData(colision, "name", zone['name'])
	end
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), zoneInit)

function setPlayerZone(hitElement)
	if getElementType(hitElement) == "player" then
		setElementData(hitElement, "zone", source)
	end
end
addEventHandler("onColShapeHit", getRootElement(), setPlayerZone)