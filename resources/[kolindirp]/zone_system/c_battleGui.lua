DGS = exports.dgs
inBattle = false
--battleGUI = {}

function createBattleWindow(zoneName, attackTeamName, defendTeamName)
	time = 600
	inBattle = true
	battleGUI = {}

	battleGUI.window = DGS:dgsDxCreateWindow(0.70, 0.30, 0.30, 0.25, "Бой за "..zoneName.."", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
	battleGUI.attackLabel = DGS:dgsDxCreateLabel(0.05, 0.1, 0.90, 0.1, attackTeamName, true, battleGUI.window)
	battleGUI.attackBar = DGS:dgsDxCreateProgressBar(0.05, 0.2, 0.90, 0.1, true, battleGUI.window, nil, 0xFFFFFFFF, nil, 0xFF7B0900, false)
	battleGUI.defendLabel = DGS:dgsDxCreateLabel(0.05, 0.3, 0.90, 0.1, defendTeamName, true, battleGUI.window)
	battleGUI.defendBar = DGS:dgsDxCreateProgressBar(0.05, 0.4, 0.90, 0.1, true, battleGUI.window, nil, 0xFFFFFFFF, nil, 0xFF7B0900, false)
	battleGUI.timeLabelText = DGS:dgsDxCreateLabel(0.05, 0.6, 0.10, 0.1, "Осталось времени:", true, battleGUI.window)
	battleGUI.timeLabel = DGS:dgsDxCreateLabel(0.35, 0.6, 0.10, 0.1, time, true, battleGUI.window)
	DGS:dgsDxProgressBarSetProgress(battleGUI.attackBar, 50)
	DGS:dgsDxProgressBarSetProgress(battleGUI.defendBar, 50)
	--setTimer(updateClientTime, 1000, 600)
end
addEventHandler("onPlayerInBattleZone", getRootElement(), createBattleWindow)

function destroyBattleWindow()
	destroyElement(battleGUI.window)
	battleGUI.window = nil
end
addEventHandler("onPlayerBattleEnd", getRootElement(), destroyBattleWindow)

function updateInfo(attack, defend, time)
	if battleGUI.window ~= nil then
		local attack = tonumber(attack)
		local defend = tonumber(defend)
		time = 600 - tonumber(time)
		DGS:dgsDxGUISetText(battleGUI.timeLabel, tostring(time))
		if attack > 79 then
			destroyElement(battleGUI.attackBar)
			battleGUI.attackBar = DGS:dgsDxCreateProgressBar(0.05, 0.2, 0.90, 0.1, true, battleGUI.window, nil, 0xFFFFFFFF, nil, 0xFF10F400, true)
			DGS:dgsDxProgressBarSetProgress(battleGUI.attackBar, attack)
		else
			DGS:dgsDxProgressBarSetProgress(battleGUI.attackBar, attack)
		end
		if defend > 79 then
			destroyElement(battleGUI.defendBar)
			battleGUI.defendBar = DGS:dgsDxCreateProgressBar(0.05, 0.2, 0.90, 0.1, true, battleGUI.window, nil, 0xFFFFFFFF, nil, 0xFF10F400, true)
			DGS:dgsDxProgressBarSetProgress(battleGUI.defendBar, defend)
		else
			DGS:dgsDxProgressBarSetProgress(battleGUI.defendBar, defend)
		end
	end
end
addEventHandler("onServerUpdateBattleInfo", getRootElement(), updateInfo)


--function updateClientTime()
	--time = 600 - tonumber(time)
	--DGS:dgsDxGUISetText(battleGUI.timeLabel, tostring(time))
--end