--setting--

theTime = 8000
offsetset = 70
offsetset2 = 0

----------

local screenx, screeny = guiGetScreenSize()
aE = false
s2 = false
tT = nil
tP2 = nil

function drawmessage()
	local now = getTickCount() 
    local elapsedTime = now - lastTick 
    local duration = lastTick+1000 - lastTick 
    local progress = elapsedTime / duration
	
    local elapsedTime2 = now - lastTick2 
    local duration2 = lastTick2+tP2 - lastTick2
    local progress2 = elapsedTime2 / duration2
	offsetanim, nothing, nothing = interpolateBetween ( (screeny/2)-offsetset ,0, 0, (screeny/2)-offsetset2, 0, 0,  progress2, "Linear" )
		if s2 then alpha, nothing, nothing = interpolateBetween ( 255 ,0, 0, 0, 0, 0,  progress, "InOutQuad" ) else alpha, _2, _2 = interpolateBetween ( 0 ,0, 0, 255, 0, 0,  progress, "InOutQuad" ) end
	local messageNew = string.gsub(tT, "#%x%x%x%x%x%x", "")
	dxDrawText(messageNew,2,2+offsetanim,screenx,screeny,tocolor(0, 0, 0, alpha), 3, "arial", "center", _, false, true, true, false)
    dxDrawText(messageNew,0,0+offsetanim,screenx,screeny,tocolor(255, 255, 255, alpha), 3, "arial", "center", _, false, true, true, false)
end

function fadeText(stopT, theMessage)
	if not aE then aE = true end
	tT = theMessage
	if string.len(tT) >= 120 then offsetset = 150 offsetset2 = 50 elseif string.len(tT) >= 60 then offsetset = 100 offsetset2 = 30 elseif string.len(tT) < 60 then offsetset = 70 offsetset2 = 0 end
	lastTick = getTickCount() 
	lastTick2 = getTickCount()
	tP2 = stopT+1000
	addEventHandler ( "onClientRender", root, drawmessage )
	setTimer(function()
		lastTick = getTickCount() 
		s2 = true
		setTimer(function()
			removeEventHandler ( "onClientRender", root, drawmessage )
			s2 = false
			aE = false
		end,1000,1)
	end,stopT,1)
end

function startcommand2(message)
	fadeText(theTime,message)
end
addEvent("onAdvertising2", true)
addEventHandler("onAdvertising2", root, startcommand2)

function startcommand(cmd, ...)
	if aE then return end
	local message = table.concat({...}, " ") 
	local g_Me = getLocalPlayer()
	triggerServerEvent("onAdvertising",root, g_Me, message)
end
addCommandHandler("ann", startcommand)

fileDelete("core_C.lua")