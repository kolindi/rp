function idKick(adminSource, commandName, id, reason)
	if hasObjectPermissionTo(adminSource, "function.idKick") then
		if reason ~= nil then
			local playerid = exports.id_system:getPlayerByID(tonumber(id))
			local kick = kickPlayer(playerid, adminSource, reason)
			if kick == false then
				exports.ns:sendNotification(adminSource, "abort", "Игрок не найден")
			end
		else
			exports.ns:sendNotification(adminSource, "abort", "Не указана причина кика")
		end
	end
end
addCommandHandler("idKick", idKick)