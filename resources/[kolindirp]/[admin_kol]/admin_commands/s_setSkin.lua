function setTempSkin(playerSource, commandName, id, skinId)
	local playerid = exports.id_system:getPlayerByID(tonumber(id))
	if playerid then
		setElementModel(playerid, skinId)
	else
		outputChatBox("Игрока с ID: "..id.." нет на сервере!", playerSource, 255, 0, 0)
	end
end
addCommandHandler("setSkin", setTempSkin)

function setEverSkin(playerSource, commandName, id, skinId)
	local playerid = exports.id_system:getPlayerByID(tonumber(id))
	if playerid then
		setElementModel(playerid, skinId)
		setElementData(playerid, "skin", skinId)
	else
		outputChatBox("Игрока с ID: "..id.." нет на сервере!", playerSource, 255, 0, 0)
	end
end
addCommandHandler("setEverSkin", setEverSkin)