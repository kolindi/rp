function getAllPlayerInfo(adminSource, commandName, id)
	if hasObjectPermissionTo(adminSource, "command.accInfo") then
		thePlayer = exports.id_system:getPlayerByID(id)
		playerInfo = getAllElementData(adminSource)
		triggerClientEvent("onAdminGetAccInfo", adminSource, playerInfo, id)
	else
		outputChatBox("afa")
	end
end
addCommandHandler("accInfo", getAllPlayerInfo)
--[[
function getMyData ( thePlayer, command )
    local data = getAllElementData ( thePlayer )     -- get all the element data of the player who entered the command
    for k, v in pairs ( data ) do                    -- loop through the table that was returned
        outputConsole ( k .. ": " .. tostring ( v ) )             -- print the name (k) and value (v) of each element data, we need to make the value a string, since it can be of any data type
    end
end
addCommandHandler ( "elemdata", getMyData )]]