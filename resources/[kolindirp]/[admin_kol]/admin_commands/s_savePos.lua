function savePosition(adminSource, commandName)
	local x, y, z = getElementPosition(adminSource)
	local rx, ry, rz = getElementRotation(adminSource, "ZYX")
	local posFile = fileOpen("savePos.txt")
	fileWrite(posFile, ""..x..", "..y..", "..z.."\n"..rx..", "..ry..", "..rz.."")
	fileClose(posFile)
	outputChatBox("Координаты сохранены в savePos.txt!", adminSource)
end
addCommandHandler("savePos", savePosition)