function realTime()
	realtime = getRealTime()
	setTime(realtime.hour, realtime.minute)
	setMinuteDuration(60000)
	setTimer(payday, 600000, 0)
end
addEventHandler("onResourceStart", getResourceRootElement(), realTime)

function payday()
	local realtime = getRealTime()
	outputChatBox("Время: "..realtime.hour.." часов "..realtime.minute.." минут "..realtime.second.." секунд")
	triggerEvent("payday", root)
end