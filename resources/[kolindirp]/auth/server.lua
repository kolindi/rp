database = call(getResourceFromName("db"), "getDb")
addEvent("PlayerLogin", true)
addEventHandler("PlayerLogin", root, 
	function(name, pass)
		local query = dbQuery(database, "SELECT * FROM accounts WHERE name = '"..name.."' AND password = '"..pass.."';")
		local result, num_rows = dbPoll(query, -1)
		if num_rows > 0 then
			for rowNum, rowData in ipairs(result) do
				if rowData['serialcheck'] == 1 then
					if getPlayerSerial(source) == rowData['serial'] then
						setElementData(source, "serialcheck", 1)
						setElementData(source, "org", rowData['org_name'])
						setElementData(source, "rank", tonumber(rowData['rank']))
						local org = getTeamFromName(getElementData(source, "org"))
						setPlayerTeam(source, org)
						setElementData(source, "paydayBankAccount", tonumber(rowData['paydayBankAccount']))
						setElementData(source, "id", tonumber(rowData['id']))
						setElementData(source, "name", name)
						setElementData(source, "lang", rowData['language'])
						setElementData(source, "number", tonumber(rowData['number']))
						setElementData(source, "sex", tonumber(rowData['sex']))
						setElementData(source, "skin", tonumber(rowData['skin']))
						setElementData(source, "money", tonumber(rowData['money']))
						setElementData(source, "stat_hack", rowData['stat_hack'])
						triggerEvent("createInventory", source, rowData['inventory'])
						if rowData['house_id'] ~= false then
							setElementData(source, "house_id", rowData['house_id'])
							setElementData(source, "car_id", rowData['car_id'])
						else
							setElementData(source, "house_id", 0)
						end
					else
						kickPlayer(source, "Серийный номер не совпадает с аккаунтом. Обратитесь на форум.")
					end
				else
					setElementData(source, "serialcheck", 0)
					setElementData(source, "org", rowData['org_name'])
					setElementData(source, "rank", tonumber(rowData['rank']))
					local org = getTeamFromName(getElementData(source, "org"))
					setPlayerTeam(source, org)
					setElementData(source, "id", rowData['id'])
					setElementData(source, "name", name)
					setElementData(source, "sex", rowData['sex'])
					setElementData(source, "skin", rowData['skin'])
					setElementData(source, "money", rowData['money'])
					setElementData(source, "stat_hack", rowData['stat_hack'])
					triggerEvent("createInventory", source, rowData['inventory'])
					if rowData['house_id'] ~= nil then
						setElementData(source, "house_id", rowData['house_id'])
						setElementData(source, "car_id", rowData['car_id'])
					else
						setElementData(source, "house_id", 0)
					end
				end
			end
			setPlayerName(source, name)
			local localAccount = getAccount(name)
			logIn(source, localAccount, pass)
			spawnPlayer(source, 0, 0, 5)
			triggerEvent("onPlayerAuth", source)
			triggerClientEvent(source, "destroyLoginPanel", source)
			showCursor(source, false)
			setCameraTarget(source, source)
			setPlayerMoney(source, getElementData(source, "money"))
			setElementModel(source, getElementData(source, "skin"))
			setElementData(source, "auth", true)
			dbFree(query)
			exports.ns:sendNotification(source, "success", "Успешная авторизация!")
		else
			exports.ns:sendNotification(source, "abort", "Введены неверные данные!")
			dbFree(query)
		end
	end
)

addEvent("PlayerRegistration", true)
addEventHandler("PlayerRegistration", root,
	function(name, pass, sex)
		local query = dbQuery(database, "SELECT * FROM accounts WHERE name = '"..name.."';")
		local result, num_rows = dbPoll(query, -1)
		if num_rows > 0 then
			outputChatBox(source, "Это имя уже зарегистрировано!")
			dbFree(query)
		else
			local serial = getPlayerSerial(source)
			local query = dbExec(database, "INSERT INTO accounts (name, password, serial, sex) VALUES ('"..tostring(name).."', '"..tostring(pass).."', '"..tostring(serial).."', '"..tonumber(sex).."');")
			addAccount(name, pass, false)
			triggerClientEvent(source, "destroyRegPanel", source)
			setPlayerName(source, name)
			spawnPlayer(source, 0, 0, 50)
			setCameraTarget(source, source)
			triggerClientEvent(source, "skinSelect", source, sex, name)
			outputDebugString("Успешная регистрация!", source)
			setElementData(source, "name", name)
			setElementData(source, "sex", sex)
			logIn(source, getAccount(name), pass)
		end
	end
)

addEvent("skinSelected", true)
addEventHandler("skinSelected", root,
	function(skin, regSkill, name)
		local query = dbExec(database, "UPDATE accounts SET skin = '"..skin.."' WHERE name = '"..name.."';")
		setElementData(source, "skin", skin)
		if (regSkill == 1) then
			local query = dbExec(database, "UPDATE accounts SET money = '"..tonumber(50000).."' WHERE name = '"..name.."';")
			setElementData(source, "money", 50000)
			setPlayerMoney(source, 50000)
		end
		triggerClientEvent(source, "regEnd", source)
	end
)

function authStarted()
	outputDebugString("Система авторизации/регистрации запущена!")
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), authStarted)

function playerSetLanguage()
	local query = dbQuery(database, "SELECT language FROM accounts WHERE serial = '"..getPlayerSerial(source).."';")
	local result, num_rows = dbPoll(query, -1)
	if num_rows > 0 then
		for _,data in ipairs(result) do
			setElementData(source, "lang", data['language'])
			triggerClientEvent(source, "createLoginPanel", source)
		end
	else
		triggerClientEvent(source, "createLangPanel", source, true)
	end
end
--addEventHandler("onPlayrJoin", getRootElement(), playerSetLanguage)
addEventHandler("onPlayerAuthLoad", getRootElement(), playerSetLanguage)