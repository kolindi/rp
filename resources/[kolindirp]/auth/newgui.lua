authGUI = {
    button = {},
    window = {},
    edit = {},
    radiobutton = {},
    staticimage = {},
    tabpanel = {},
    tab = {},
    label = {}
}

--local avalon = exports.dgs:dgsDxGUICreateFont("fonts/avalon-medium.ttf")

addEventHandler("onClientResourceStart", getResourceRootElement(getThisResource()),
	function()
		fadeCamera(true)
		setCameraMatrix(150, 0, 250)
		showCursor(true)
		authGUI.loginWindow = exports.dgs:dgsDxCreateWindow(0.74, 0.33, 0.25, 0.35, "Авторизация", true, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		exports.dgs:dgsDxWindowSetMovable(authGUI.loginWindow, false)
		exports.dgs:dgsDxWindowSetSizable(authGUI.loginWindow, false)
		--guiSetAlpha(authGUI.loginWindow, 0.80)

		authGUI.edit[1] = exports.dgs:dgsDxCreateEdit(0.04, 0.13, 0.93, 0.09, "Имя_Фамилия", true, authGUI.loginWindow)
		--exports.dgs:dgsDxEditSetMaxLength(authGUI.edit[1], 32)
		authGUI.edit[2] = exports.dgs:dgsDxCreateEdit(0.04, 0.26, 0.93, 0.08, "Пароль", true, authGUI.loginWindow)
		--exports.dgs:dgsDxEditSetMaxLength(authGUI.edit[2], 32)
		authGUI.button[1] = exports.dgs:dgsDxCreateButton(0.26, 0.41, 0.47, 0.18, "Войти", true, authGUI.loginWindow)
		--exports.dgs:dgsDxGUISetFont(authGUI.button[1], "default-bold-small")
		authGUI.button[2] = exports.dgs:dgsDxCreateButton(0.26, 0.74, 0.47, 0.18, "Создать аккаунт", true, authGUI.loginWindow)
		--exports.dgs:dgsDxGUISetFont(authGUI.button[2], "default-bold-small")

		addEventHandler("onClientDgsDxMouseClick", getRootElement(), 
			function()
				if source == authGUI.edit[1] then
					exports.dgs:dgsDxGUISetText(authGUI.edit[1], "")
				elseif source == authGUI.edit[2] then
					exports.dgs:dgsDxGUISetText(authGUI.edit[2], "")
					--guiEditSetMasked(authGUI.edit[2], true)
				elseif source == authGUI.button[1] then
					local name, pass = exports.dgs:dgsDxGUIGetText(authGUI.edit[1]), exports.dgs:dgsDxGUIGetText(authGUI.edit[2])
					if (name == "") or (pass == "") then
						outputChatBox("Не заполнены данные для входа!")
					else
						triggerServerEvent("PlayerLogin", getLocalPlayer(), name, pass)
					end
				elseif source == authGUI.button[2] then
					destroyElement(authGUI.loginWindow)
					local zayva = exports.dgs:dgsDxImageLoadImage("img/zayva")
					authGUI.staticimage[1] = exports.dgs:dgsDxCreateImage(0.55, 0.22, 0.45, 0.71, zayva, true)

					authGUI.edit[3] = exports.dgs:dgsDxCreateEdit(0.12, 0.29, 0.24, 0.03, "", true, authGUI.staticimage[1])
					exports.dgs:dgsDxGUISetFont(authGUI.edit[3], "avalon")
					authGUI.edit[4] = exports.dgs:dgsDxCreateEdit(0.18, 0.54, 0.25, 0.02, "", true, authGUI.staticimage[1])
					authGUI.radiobutton[1] = exports.dgs:dgsDxCreateRadioButton(0.19, 0.63, 0.03, 0.02, "", true, authGUI.staticimage[1])
					exports.dgs:dgsDxRadioButtonSetSelected(authGUI.radiobutton[1], true)
					authGUI.radiobutton[2] = exports.dgs:dgsDxCreateRadioButton(0.32, 0.63, 0.03, 0.02, "", true, authGUI.staticimage[1])
					authGUI.button[3] = exports.dgs:dgxDxCreateButton(0.15, 0.74, 0.30, 0.15, "Подписать", true, authGUI.staticimage[1])
					--guiSetFont(authGUI.button[3], "default-bold-small")
					addEventHandler("onClientDgsDxMouseClick", getResourceRootElement(getThisResource()),
						function()
							if source == authGUI.button[3] then
								local name, pass, serial = exports.dgs:dgsDxGUIGetText(authGUI.edit[3]), exports.dgs:dgsDxGUIGetText(authGUI.edit[4]), getPlayerSerial()
								if (exports.dgs:dgsDxRadioButtonGetSelected(authGUI.radiobutton[1])) then
									sex = 1
									outputDebugString(sex)
								elseif (exports.dgs:dgsDxRadioButtonGetSelected(authGUI.radiobutton[2])) then
									sex = 2
									outputDebugString(sex)
								end
								if (name == "") or (pass == "") or (sex == nil) then
									outputChatBox("Не все поля заполнены!")
								else
									triggerServerEvent("PlayerRegistration", getLocalPlayer(), name, pass, serial, sex)
								end
							elseif source == authGUI.radiobutton[1] or source == authGUI.radiobutton[2] then
								playSound("sound/galo4ka.mp3", false)
							end
						end
					)
				end
			end
		)	
	end
)

addEvent("destroyLoginPanel", true)
addEventHandler("destroyLoginPanel", getResourceRootElement(getThisResource()), 
	function()
		destroyElement(authGUI.loginWindow)
	end
)
addEvent("destroyRegPanel", true)
addEventHandler("destroyRegPanel", getResourceRootElement(getThisResource()),
	function()
		destroyElement(authGUI.staticimage[1])
	end
)

addEvent("skinSelect", true)
addEventHandler("skinSelect", getResourceRootElement(getThisResource()),
	function(sex, name)
		outputDebugString(sex)
		authGUI.skinMenu = exports.dgs:dgsDxCreateWindow(0.75, 0.31, 0.24, 0.44, "Кастомизация", true)
		exports.dgs:dgsDxWindowSetMovable(authGUI.skinMenu, false)
		exports.dgs:dgsDxWindowSetSizable(authGUI.skinMenu, false)
		
		authGUI.tabpanel[1] = exports.dgs:dgsDxCreateTabPanel(0.03, 0.06, 0.94, 0.92, true, authGUI.skinMenu)
		
		authGUI.tab[1] = exports.dgs:dgsDxCreateTab("Внешность", authGUI.tabpanel[1])
		
		authGUI.button[4] = exports.dgs:dgsDxCreateButton(0.25, 0.13, 0.54, 0.18, "Следующий", true, authGUI.tab[1])
		--guiSetFont(authGUI.button[4], "default-bold-small")
		authGUI.button[5] = exports.dgs:dgsDxCreateButton(0.24, 0.45, 0.55, 0.18, "Предыдущий", true, authGUI.tab[1])
		--guiSetFont(authGUI.button[5], "default-bold-small")
		
		authGUI.tab[2] = exports.dgs:dgsDxCreateTab("Умение", authGUI.tabpanel[1])
		
		authGUI.label[1] = exports.dgs:dgsDxCreateLabel(0.13, 0.03, 0.71, 0.05, "Вы можете выбрать одно умение", true, authGUI.tab[2])
		--guiSetFont(authGUI.label[1], "default-bold-small")
		authGUI.radiobutton[3] = exports.dgs:dgsDxCreateRadioButton(0.61, 0.15, 0.06, 0.04, "", true, authGUI.tab[2])
		authGUI.label[2] = exports.dgs:dgsDxCreateLabel(0.02, 0.15, 0.50, 0.04, "Стрельба из пистолетов", true, authGUI.tab[2])
		authGUI.label[3] = exports.dgs:dgsDxCreateLabel(0.03, 0.70, 0.49, 0.09, "50000Kol\n(Колиндов - валюта)", true, authGUI.tab[2])
		authGUI.radiobutton[4] = exports.dgs:dgsDxCreateRadioButton(0.62, 0.72, 0.05, 0.04, "", true, authGUI.tab[2])
		exports.dgs:dgsDxRadioButtonSetSelected(authGUI.radiobutton[4], true)
		
		authGUI.button[6] = exports.dgs:dgsDxCreateButton(0.19, 0.83, 0.59, 0.12, "Готово!", true, authGUI.tab[1])
		authGUI.button[7] = exports.dgs:dgsDxCreateButton(0.19, 0.83, 0.59, 0.12, "Готово!", true, authGUI.tab[2])
		--guiSetFont(authGUI.button[6], "default-bold-small")
		--guiSetFont(authGUI.button[7], "default-bold-small")

		maleSkin = {
			[1] = 1,
        	[2] = 2,
        	[3] = 7,
        	[4] = 14,
        	[5] = 15,
        	[6] = 19,
        	[7] = 20,
        	[8] = 21,
        	[9] = 22,
        	[10] = 23,
        	[11] = 24,
        	[12] = 25,
        	[13] = 28,
        	[14] = 29,
        	[15] = 30,
        	[16] = 32,
        	[17] = 34,
        	[18] = 35,
        	[19] = 36,
        	[20] = 37,
        	[21] = 43,
        	[22] = 44,
        	[23] = 47,
        	[24] = 50,
        	[25] = 60,
        	[26] = 66,
        	[27] = 67,
        	[28] = 72,
        	[29] = 73,
        	[30] = 78,
        	[31] = 79,
        	[32] = 82,
        	[33] = 83,
        	[34] = 84,
        	[35] = 96,
        	[36] = 98,
        	[37] = 101,
        	[38] = 121,
        	[39] = 128,
        	[40] = 132,
        	[41] = 133,
        	[42] = 134,
        	[43] = 135,
        	[44] = 136,
        	[45] = 137,
        	[46] = 142
		}
		womanSkin = {
			[1] = 9,
    		[2] = 12,
    		[3] = 13,
    		[4] = 31,
    		[5] = 38,
    		[6] = 41,
    		[7] = 53,
    		[8] = 54,
    		[9] = 55,
    		[10] = 56,
    		[11] = 69,
    		[12] = 75,
    		[13] = 77,
    		[14] = 90,
    		[15] = 93,
    		[16] = 151,
    		[17] = 152,
    		[18] = 157,
    		[19] = 193,
    		[20] = 201
		}
		local k = 1
		local player = getLocalPlayer()
		if sex == 1 then
			setElementModel(player, maleSkin[k])
		elseif sex == 2 then
			setElementModel(player, womanSkin[k])
		else
			outputChatBox("ОШИБКА. Пол не определён!")
		end

		addEventHandler("onClientDgsDxMouseClick", getResourceRootElement(getThisResource()),
			function()
				if source == authGUI.button[4] then
					outputDebugString(sex)
					if k ~= 46 and sex == 1 then
						k = k + 1
						setElementModel(player, maleSkin[k])
					elseif k == 46 and sex == 1 then
						k = 1
						setElementModel(player, maleSkin[k])
					elseif k ~= 20 and sex == 2 then
						k = k + 1
						setElementModel(player, womanSkin[k])
					elseif k == 20 and sex == 2 then
						k = 1
						setElementModel(player, womanSkin[k])
					else
						outputChatBox("ОШИБКА! Попробуйте заново")
					end
				elseif source == authGUI.button[5] then
					if k ~= 1 and sex == 1 then
						k = k - 1
						setElementModel(player, maleSkin[k])
					elseif k == 1 and sex == 1 then
						k = 46
						setElementModel(player, maleSkin[k])
					elseif k ~= 1 and sex == 2 then
						k = k - 1
						setElementModel(player, womanSkin[k])
					elseif k == 1 and sex == 2 then
						k = 20
						setElementModel(player, womanSkin[k])
					end
				elseif source == authGUI.button[6] or source == authGUI.button[7] then
					local skin = getElementModel(player)
					if (exports.dgs:dgsDxRadioButtonGetSelected(authGUI.radiobutton[4])) then
						regSkill = 1
					elseif (exports.dgs:dgsDxRadioButtonGetSelected(authGUI.radiobutton[3])) then
						outputDebugString("В разработке!")
						regSkill = 2
					end
					triggerServerEvent("skinSelected", getLocalPlayer(), skin, regSkill, name)
				end
			end
		)
	end
)

addEvent("regEnd", true)
addEventHandler("regEnd", getResourceRootElement(getThisResource()),
	function()
		destroyElement(authGUI.skinMenu)
		showCursor(false)
	end
)